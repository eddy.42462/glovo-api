django==3.1
djangorestframework==3.11.0
requests==2.24.0
djangorestframework-xml==2.0.0
psycopg2-binary==2.8.6
drf-yasg==1.17.1
django-celery-beat==2.1.0
celery==5.0.1
redis==3.5.3
jsonschema==3.2.0
django-bulk-update-or-create==0.2.0
djangorestframework-datatables==0.6.0
Pillow==2.2.1


