from django.http import HttpResponse

from .error_type import ErrorType
from .request_error import RequestError


class BadRequest(HttpResponse):
    def __init__(self, http_status_code, exception):
        """
        """
        if isinstance(exception, RequestError):
            super(BadRequest, self).__init__(content=exception)
        else:
            text_message = self.clean_text(str(exception))
            request_error = RequestError(error_type=ErrorType.ERROR, message=text_message)
            super(BadRequest, self).__init__(content=request_error)
        self.status_code = http_status_code

    def clean_text(self, text):
        exception_str = text
        exception_str = exception_str.replace('[', '')
        exception_str = exception_str.replace(']', '')
        exception_str = exception_str.replace("'", '')
        return exception_str
