from datetime import datetime, timedelta

import requests
from celery import shared_task
from django.conf import settings
from rest_framework import status
from store.enum import ModeEnum, TraceTypeEnum, AggregatorEnum, OrderStatusFinalEnum, OrderStatusRappiEnum, \
    OrderCancelTypeEnum
from store.models import Store, Aggregator, StoreAggregator
from store.utils import get_headers_by_aggregator, insert_trace

from .controllers.order_controller import OrderController
from .controllers.order_response_body_controller import OrderResponseBodyController
from .models import Order, OrderCanceled


@shared_task
def update_orders_status_final():
    """
    Task run periodically that allows update the status_final attribute in the orders
    """
    date = datetime.now() - timedelta(minutes=20)
    date2 = datetime.now() - timedelta(minutes=5)

    aggregator_rappi = None
    try:
        aggregator_rappi = Aggregator.objects.get(name=AggregatorEnum.RAPPI.value)
    except Exception as e:
        print(str(e))
        exception = "Aggregator with name {} do not exist".format(AggregatorEnum.RAPPI.value) + "Exception: " + str(e)
        insert_trace(trace_type=TraceTypeEnum.ORDER_CANCEL_BY_AGGREGATOR.value,
                     instance_id=AggregatorEnum.RAPPI.value,
                     exception=exception)

    orders = Order.objects.filter(created_at__range=[date, date2],
                                  aggregator=aggregator_rappi,
                                  status_final=None)

    print('Econtradas a cancelar: {}'.format(len(orders)))

    if orders:
        orders_to_update = []
        order_caneled_to_insert = []
        for order in orders:
            order.status = OrderStatusRappiEnum.REJECTED.value
            order.status_final = OrderStatusFinalEnum.CANCELED.value
            orders_to_update.append(order)

            order_canceled = OrderCanceled(order=order, cancel_type=OrderCancelTypeEnum.AGGREGATOR.value)
            order_caneled_to_insert.append(order_canceled)

        Order.objects.bulk_update(orders, ['status_final', 'status'])
        OrderCanceled.objects.bulk_create(order_caneled_to_insert)


@shared_task
def find_new_orders():
    """
    Task run periodically that allows searching for new orders in Rappi aggregator
    """
    order_ctl = OrderController()
    order_response_ctl = OrderResponseBodyController()

    for store in Store.objects.filter(aggregator__name=AggregatorEnum.RAPPI.value):
        store_aggregators = StoreAggregator.objects.filter(store=store, aggregator__name=AggregatorEnum.RAPPI.value)
        store_aggregator = None
        if store_aggregators:
            store_aggregator = store_aggregators[0]

        print('-' * 80)
        print('TIENDA: ' + store.integrationId)
        aggregator_inserted = None
        try:
            aggregator_inserted = Aggregator.objects.get(name=AggregatorEnum.RAPPI.value)
        except Exception as e:
            exception = "Not finded new Orders in aggregator Rappi because Aggregator with name {} do not exist".format(
                AggregatorEnum.RAPPI.value) + "Exception: " + str(e)
            insert_trace(trace_type=TraceTypeEnum.GET_NEW_ORDERS.value,
                         instance_id=AggregatorEnum.RAPPI.value,
                         exception=exception)

        if aggregator_inserted and store_aggregator and store.production_mode and aggregator_inserted.production_url:

            headers = get_headers_by_aggregator(AggregatorEnum.RAPPI.value, store, ModeEnum.PRODUCTION,
                                                store_aggregator)

            rappi_orders_production_url = aggregator_inserted.production_url + '/orders'
            #rappi_orders_production_url = settings.LOCAL_GET_OREDERS_FAKE_URL

            # ------------- buscando las nuevas ordenes en Rappi ----------------------------
            try:
                response_rappi = requests.get(url=rappi_orders_production_url, headers=headers)
            except Exception as e:
                insert_trace(trace_type=TraceTypeEnum.GET_NEW_ORDERS.value,
                             exception=str(e),
                             mode=ModeEnum.PRODUCTION.value)
                return None
            # -------------------------------------------------------------------------------

            if response_rappi.status_code == status.HTTP_200_OK:
                print('PRODUCTION: ' + str(response_rappi.json()))

                if response_rappi.json():

                    order_response_ctl.insert_order_response_body_from_rappi(store, aggregator_inserted,
                                                                             response_rappi.json(),
                                                                             ModeEnum.PRODUCTION)

                    for order_data in response_rappi.json():
                        order_ctl.insert_order_from_rappi(store=store,
                                                          aggregator=aggregator_inserted,
                                                          order_data=order_data,
                                                          mode=ModeEnum.PRODUCTION)

            else:
                exception = 'Not received new Orders from Rappi aggregator because bad status response was received, details: {}'.format(
                    response_rappi.content)
                insert_trace(trace_type=TraceTypeEnum.GET_NEW_ORDERS.value,
                             exception=exception,
                             mode=ModeEnum.PRODUCTION.value)
                print("Response of request to get orders has taken status {}".format(
                    response_rappi.status_code))

        if aggregator_inserted and store_aggregator and store.develop_mode and aggregator_inserted.develop_url:
            headers = get_headers_by_aggregator(AggregatorEnum.RAPPI.value, store, ModeEnum.DEVELOP,
                                                store_aggregator)

            rappi_develop_orders_url = aggregator_inserted.develop_url + '/orders'
            #rappi_develop_orders_url = settings.LOCAL_GET_OREDERS_FAKE_URL

            # ------------- buscando las nuevas ordenes en Rappi ----------------------------
            try:
                response_rappi = requests.get(url=rappi_develop_orders_url, headers=headers)
            except Exception as e:
                insert_trace(trace_type=TraceTypeEnum.GET_NEW_ORDERS.value,
                             exception=str(e),
                             mode=ModeEnum.DEVELOP.value)
                return None
            # -------------------------------------------------------------------------------

            if response_rappi.status_code == status.HTTP_200_OK:
                print('DEVELOP: ' + str(response_rappi.json()))

                if response_rappi.json():

                    order_response_ctl.insert_order_response_body_from_rappi(store, aggregator_inserted,
                                                                             response_rappi.json(),
                                                                             ModeEnum.DEVELOP)

                    for order_data in response_rappi.json():
                        order_ctl.insert_order_from_rappi(store=store,
                                                          aggregator=aggregator_inserted,
                                                          order_data=order_data,
                                                          mode=ModeEnum.DEVELOP)

            else:
                exception = 'Not received new Orders from Rappi aggregator because bad status response was received, details: {}'.format(
                    response_rappi.content)
                insert_trace(trace_type=TraceTypeEnum.GET_NEW_ORDERS.value,
                             exception=exception,
                             mode=ModeEnum.DEVELOP.value)
                print("Response of request to get orders has taken status {}".format(
                    response_rappi.status_code))
