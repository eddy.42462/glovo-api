from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import JSONField
from store.models import Store, Aggregator
from store.enum import OrderStatusRappiEnum, OrderStatusPixelEnum, OrderStatusFinalEnum, ModeEnum
from .utils import validate_date_format

ORDER_STATUSES_RAPPI = (
    (OrderStatusRappiEnum.CREATED.value, 'Created'),
    (OrderStatusRappiEnum.READY.value, 'Ready'),
    (OrderStatusRappiEnum.SENT.value, 'Sent'),
    (OrderStatusRappiEnum.TAKEN.value, 'Taken'),
    (OrderStatusRappiEnum.REJECTED.value, 'Rejected'),
    (OrderStatusRappiEnum.TIMEOUT.value, 'Timeout'),
    (OrderStatusRappiEnum.READY_FOR_PICKUP.value, 'Ready for pickup'),
)

ORDER_STATUSES_PIXEL = (
    (OrderStatusPixelEnum.CREATED.value, 'Created'),
    (OrderStatusPixelEnum.SENT.value, 'Sent'),
    (OrderStatusPixelEnum.PROCESSED.value, 'Processed'),
    (OrderStatusPixelEnum.REJECTED.value, 'Rejected'),
)

ORDER_STATUSES_FINAL = (
    (OrderStatusFinalEnum.SUCCESS.value, 'Success'),
    (OrderStatusFinalEnum.CANCELED.value, 'Canceled'),
)

CANCEL_REASON = (
    ('PRODUCTS_NOT_AVAILABLE', 'Product not available'),
    ('STORE_CAN_NOT_DELIVER', 'Can\'t not delivery'),
    ('PARTNER_PRINTER_ISSUE', 'Partner printer issue'),
    ('USER_ERROR', 'User error'),
    ('ORDER_NOT_FEASIBLE', 'Order not feasible'),
    ('OTHER', 'Other'),
)

PAYMENT_STRATEGY = (
    ('PAY_NOTHING', 'Pay nothing'),
    ('PAY_PRODUCTS', 'Pay products'),
)


class DeliveryDiscount(models.Model):
    id_pk = models.AutoField(primary_key=True)
    total_percentage_discount = models.PositiveIntegerField(null=True)
    total_value_discount = models.PositiveIntegerField(null=True)


class BillingInformation(models.Model):
    id_pk = models.AutoField(primary_key=True)
    billing_type = models.CharField(max_length=32, null=True, blank=True)
    name = models.CharField(max_length=64, null=True, blank=True)
    address = models.CharField(max_length=128, null=True, blank=True)
    phone = models.CharField(max_length=64, null=True, blank=True)
    email = models.CharField(max_length=64, null=True, blank=True)
    document_type = models.CharField(max_length=32, null=True, blank=True)
    document_number = models.CharField(max_length=32, null=True, blank=True)

    def __str__(self):
        return self.billing_type + self.name


class Customer(models.Model):
    id_pk = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64, null=True, blank=True)
    phone_number = models.CharField(max_length=64, null=True, blank=True)
    hash = models.CharField(max_length=64, null=True, blank=True)
    first_name = models.CharField(max_length=64, null=True, blank=True)
    last_name = models.CharField(max_length=64, null=True, blank=True)
    user_type = models.CharField(max_length=64, null=True, blank=True)
    email = models.CharField(max_length=64, null=True, blank=True)
    document_number = models.CharField(max_length=32, null=True, blank=True)

    def __str__(self):
        return self.name

    @property
    def invoicings(self):
        return self.invoice_set.all()

    @property
    def orders(self):
        return self.order_set.all()


class Courier(models.Model):
    id_pk = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64, null=True, blank=True)
    phone_number = models.CharField(max_length=64, blank=True)

    def __str__(self):
        return self.name

    @property
    def orders(self):
        return self.order_set.all()


class DeliveryInformation(models.Model):
    id_pk = models.AutoField(primary_key=True)
    # Glovo
    label = models.CharField(max_length=64, null=True, blank=True)
    longitude = models.CharField(max_length=64, null=True, blank=True)
    latitude = models.CharField(max_length=64, null=True, blank=True)
    # Rappi
    city = models.CharField(max_length=32, null=True, blank=True)
    complete_address = models.CharField(max_length=128, null=True, blank=True)
    description = models.CharField(max_length=64, null=True, blank=True)
    neighborhood = models.CharField(max_length=32, null=True, blank=True)
    postal_code = models.CharField(max_length=32, null=True, blank=True)
    complement = models.CharField(max_length=32, null=True, blank=True)

    def __str__(self):
        return self.label

    @property
    def order(self):
        return self.order


class Invoice(models.Model):
    id_pk = models.AutoField(primary_key=True)
    company_name = models.CharField(max_length=64, null=True, blank=True)
    company_address = models.CharField(max_length=64, null=True, blank=True)
    tax_id = models.CharField(max_length=64, null=True, blank=True)
    customer = models.ForeignKey(Customer, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.company_name

class OrderActivesManager(models.Manager):
    def get_queryset(self):
        return super(OrderActivesManager, self).get_queryset().filter(status_final=None)

class OrderSuccessManager(models.Manager):
    def get_queryset(self):
        status_final = OrderStatusFinalEnum.SUCCESS.value
        return super(OrderSuccessManager, self).get_queryset().filter(status_final=status_final)

class OrderCanceledManager(models.Manager):
    def get_queryset(self):
        status_final = OrderStatusFinalEnum.CANCELED.value
        return super(OrderCanceledManager, self).get_queryset().filter(status_final=status_final)

class Order(models.Model):
    id_pk = models.AutoField(primary_key=True)
    order_id = models.CharField(max_length=64)
    order_json = JSONField()
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    order_time = models.CharField(max_length=64, validators=[validate_date_format], null=True, blank=True)
    estimated_pickup_time = models.CharField(max_length=64, validators=[validate_date_format], null=True, blank=True)
    utc_offset_minutes = models.CharField(max_length=64, null=True, blank=True)
    currency = models.CharField(max_length=64, null=True, blank=True)
    order_code = models.CharField(max_length=64, null=True, blank=True)
    allergy_info = models.CharField(max_length=64, null=True, blank=True)
    estimated_total_price = models.PositiveIntegerField(null=True)
    delivery_fee = models.PositiveIntegerField(null=True)
    minimum_basket_surcharge = models.PositiveIntegerField(null=True)
    customer_cash_payment_amount = models.PositiveIntegerField(null=True)
    bundled_orders = ArrayField(models.CharField(max_length=64), null=True)
    pick_up_code = models.CharField(max_length=64, null=True, blank=True)
    is_picked_up_by_customer = models.BooleanField(null=True)
    cutlery_requested = models.BooleanField(null=True)
    total_customer_to_pay = models.FloatField(null=True)
    status = models.PositiveIntegerField(null=True)
    status_pixel = models.PositiveIntegerField(null=True)
    status_final = models.PositiveIntegerField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    processed_at = models.DateTimeField(null=True)
    processed_at_pixel = models.DateTimeField(null=True)
    transaction_number = models.CharField(max_length=16, null=True)
    # Rappi
    created_at_rappi = models.DateTimeField(null=True)
    cooking_time = models.PositiveIntegerField(null=True)
    delivery_method = models.CharField(max_length=64, null=True, blank=True)
    min_cooking_time = models.PositiveIntegerField(null=True)
    max_cooking_time = models.PositiveIntegerField(null=True)
    payment_method = models.CharField(max_length=64, blank=True)
    total_products_with_discount = models.FloatField(null=True)
    total_products_without_discount = models.FloatField(null=True)
    total_other_discounts = models.FloatField(null=True)
    total_order = models.FloatField(null=True)
    total_to_pay = models.FloatField(null=True)
    total_rappi_credits = models.FloatField(null=True)
    total_rappi_pay = models.FloatField(null=True)
    tip = models.FloatField(null=True)
    shipping = models.FloatField(null=True)  # costo de envio domicilio
    service_fee = models.FloatField(null=True)  # servicio
    # foreign_keys
    aggregator = models.ForeignKey(Aggregator, on_delete=models.CASCADE)
    courier = models.ForeignKey(Courier, on_delete=models.CASCADE, null=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=True)
    delivery_discount = models.ForeignKey(DeliveryDiscount, on_delete=models.CASCADE, null=True, blank=True)
    billing_information = models.ForeignKey(BillingInformation, on_delete=models.CASCADE, null=True, blank=True)
    delivery_information = models.OneToOneField(DeliveryInformation, on_delete=models.CASCADE, null=True)
    mode = models.IntegerField(default=ModeEnum.PRODUCTION.value)

    orders_success = OrderSuccessManager()
    orders_canceled = OrderCanceledManager()
    orders_actives = OrderActivesManager()
    objects = models.Manager()

    def __str__(self):
        return 'Order:' + self.order_id

    @property
    def order_canceled(self):
        return self.order_canceled


class OrderCanceled(models.Model):
    id_pk = models.AutoField(primary_key=True)
    order = models.OneToOneField(Order, related_name='order_canceled', on_delete=models.CASCADE)
    cancel_type = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)


class ProductOrder(models.Model):
    id_pk = models.AutoField(primary_key=True)
    id = models.CharField(max_length=32, null=True)  # este campo identifica al producto en el agregador
    name = models.CharField(max_length=64, null=True, blank=True)
    price = models.CharField(max_length=16, null=True, blank=True)
    quantity = models.PositiveIntegerField(null=True)
    purchased_product_id = models.CharField(max_length=32, null=True, blank=True)
    # Rappi
    parent_item_id = models.CharField(max_length=32, null=True)
    sku = models.CharField(max_length=32, null=True)
    type = models.CharField(max_length=32, null=True, blank=True)
    comments = models.CharField(max_length=64, null=True, blank=True)
    unit_price_with_discount = models.FloatField(null=True)
    unit_price_without_discount = models.FloatField(null=True)
    percentage_discount = models.FloatField(null=True)
    order = models.ForeignKey(Order, related_name='products', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class OrderCancelation(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    cancel_reason = models.CharField(max_length=22, choices=CANCEL_REASON)
    payment_strategy = models.CharField(max_length=12, choices=PAYMENT_STRATEGY)


class OrderBodyResponse(models.Model):
    id_pk = models.AutoField(primary_key=True)
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    aggregator = models.ForeignKey(Aggregator, on_delete=models.CASCADE)
    orders_json = JSONField()
    created_at = models.DateTimeField(auto_now_add=True)


class OrderNotReceived(models.Model):
    id = models.AutoField(primary_key=True)
    order_id = models.CharField(max_length=64)
    store_id = models.PositiveIntegerField(null=True)
    aggregator_id = models.PositiveIntegerField(null=True)
    order_json = JSONField(null=True)
    exception = models.TextField(null=True)
    mode = models.IntegerField(default=ModeEnum.PRODUCTION.value)
    created_at = models.DateTimeField(auto_now_add=True)
