import json

from rest_framework.exceptions import APIException

from .error_type import ErrorType


class RequestError(Exception):
    def __init__(self, error_type, message, title=None):
        """
        """
        super(RequestError, self).__init__()
        self.error_type = error_type.value
        self.message = message
        self.title = title
        # status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        # default_detail = _('A server error occurred.')
        # default_code = 'error'

    def __str__(self):
        body = {'code': self.error_type, 'message': self.message,
                'title': self.title if self.title else self.get_default_title()}
        return json.dumps(body)

    def get_default_title(self):
        if self.error_type == ErrorType.SUCCESS.value:
            return 'Satisfactorio'
        if self.error_type == ErrorType.INFO.value:
            return 'Información'
        if self.error_type == ErrorType.WARNING.value:
            return 'Advertencia'
        if self.error_type == ErrorType.ERROR.value:
            return 'Error'
