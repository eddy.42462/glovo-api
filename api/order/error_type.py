from enum import Enum


class ErrorType(Enum):
    """
    """
    SUCCESS = 'success'
    INFO = 'info'
    WARNING = 'warning'
    ERROR = 'error'
