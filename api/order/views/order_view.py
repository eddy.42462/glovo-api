import json
from datetime import datetime, timedelta
from random import randint

import dateutil.parser
import requests
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from store.controllers.aggregator_controller import AggregatorController
from store.controllers.store_controller import StoreController
from store.dicts import dict_status_rappi, dict_status_pixel, dict_status_final, dict_mode
from store.enum import ModeEnum, TraceTypeEnum, AggregatorEnum, OrderStatusRappiEnum, \
    OrderStatusPixelEnum, OrderStatusFinalEnum, OrderCancelTypeEnum
from store.models import Store, Aggregator, StoreAggregator
from store.utils import get_headers_by_aggregator, insert_trace

from ..bad_request import BadRequest
from ..controllers.billing_information_controller import BillingInformationController
from ..controllers.customer_controller import CustomerController
from ..controllers.delivery_discount_controller import DeliveryDiscountController
from ..controllers.delivery_information_controller import DeliveryInformationController
from ..controllers.product_order_controller import ProductOrderController
from ..error_type import ErrorType
from ..models import Order, OrderNotReceived, OrderCanceled, OrderSuccessManager
from ..notifications_templates import *
from ..request_error import RequestError
from ..serializers import OrderSerializer


class OrderViewSet(viewsets.ModelViewSet):
    """
    Servicios de Order
    """
    queryset = Order.objects.all().order_by('order_id')
    serializer_class = OrderSerializer
    customer_ctl = CustomerController()
    delivery_info_ctl = DeliveryInformationController()
    product_order_ctl = ProductOrderController()
    billing_info_ctl = BillingInformationController()
    delivery_discount_ctl = DeliveryDiscountController()
    store_ctl = StoreController()
    aggregator_ctl = AggregatorController()

    @action(detail=False, url_path='actives/(?P<store_id>[^/.]+)', url_name='get_actives')
    def get_new_orders_from_aggregator(self, request, store_id) -> Response:
        """
        Descripcion: Servicio que retorna un listado de ordenes activas filtradas por los parametros recibidos

        Parametros obligatorios (path params):
        "store_id"       : identificador de la tienda, ruc + nro_establecimiento

        Parametros opcionales (request params):
        start: inicio del rango de tiempo para filtrar las ordenes activas por el atributo de la fecha de registro
        (created_at) en el API, si se especifica este parametro y no el parametro "end" devuelve entonces las ordenes
        activas con el mismo dia, mes y ano del parametro "start"

        end: fin del rango de tiempo para filtrar las ordenes activas por el atributo de la fecha de registro en el API.

        develop: si viene con valor se devuelven las ordenes activas buscadas en el ambiente de desarrollo del agregador,
        si no trae valor lsa del ambiente de produccion, si no se le especifica valor toma por defecto el ambiende de
        produccion

        aggregatorName: nombre del agregador, si se especifica se retornan solo las ordenes activas buscadas en ese
        agregador
        """
        start = request.GET.get('start', None)
        end = request.GET.get('end', None)
        aggregator_name = request.GET.get('aggregatorName', False)
        mode = ModeEnum.DEVELOP if request.GET.get('develop', False) else ModeEnum.PRODUCTION
        actives = True if request.GET.get('actives', False) else False

        try:
            store = Store.objects.get(integrationId=store_id)
        except Exception as e:
            body = {'message': NOT_EXIST_STORE_ID.format(store_id), 'details': str(e)}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        aggregator = None
        if aggregator_name:
            try:
                aggregator = Aggregator.objects.get(name=aggregator_name)
            except Exception as e:
                body = {'message': AGGREGATOR_NOT_EXIST.format(aggregator_name), 'details': str(e)}
                return Response(data=body, status=status.HTTP_404_NOT_FOUND)

            if not aggregator_name in store.aggregator.values_list('name', flat=True):
                body = {'message': NOT_RELATION_STORE_AGGREGATOR.format(aggregator_name), 'details': ''}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        if not store.active:
            body = {'message': NOT_ACTIVE_STORE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        if mode == ModeEnum.DEVELOP and not store.develop_mode:
            body = {'message': NOT_ACTIVATE_DEVELOP_MODE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        if mode == ModeEnum.PRODUCTION and not store.production_mode:
            body = {'message': NOT_ACTIVATE_PRODUCTION_MODE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        try:
            start_datetime = dateutil.parser.isoparse(start) if start else None
            end_datetime = dateutil.parser.isoparse(end) if end else None
        except Exception as e:
            body = {'message': WRONG_DATE_FORMAT, 'details': str(e)}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        if start_datetime and end_datetime:
            if actives:
                orders = Order.orders_actives.filter(created_at__range=[start_datetime, end_datetime],
                                                     store=store,
                                                     mode=mode.value)
            else:
                orders = Order.objects.filter(created_at__range=[start_datetime, end_datetime],
                                              store=store,
                                              mode=mode.value)
        elif start_datetime:
            if actives:
                orders = Order.orders_actives.filter(created_at__year=start_datetime.year,
                                                     created_at__month=start_datetime.month,
                                                     created_at__day=start_datetime.day,
                                                     store=store,
                                                     mode=mode.value)
            else:
                orders = Order.objects.filter(created_at__year=start_datetime.year,
                                              created_at__month=start_datetime.month,
                                              created_at__day=start_datetime.day,
                                              store=store,
                                              mode=mode.value)
        else:
            now = datetime.now()
            if actives:
                orders = Order.orders_actives.filter(created_at__year=now.year,
                                                     created_at__month=now.month,
                                                     created_at__day=now.day,
                                                     store=store,
                                                     mode=mode.value)
            else:
                orders = Order.objects.filter(created_at__year=now.year,
                                              created_at__month=now.month,
                                              created_at__day=now.day,
                                              store=store,
                                              mode=mode.value)
        if aggregator:
            orders.filter(aggregator=aggregator)

        serializer = OrderSerializer(instance=orders, many=True)
        try:
            orders.update(status_pixel=OrderStatusPixelEnum.SENT.value)
        except Exception as e:
            body = {'message': ERROR_UPDATE_ORDER, 'details': str(e)}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(detail=False, url_path='params/(?P<aggregator_name>[^/.]+)/(?P<store_id>[^/.]+)/(?P<order_id>[^/.]+)')
    def get_order_by_params(self, request, aggregator_name, store_id, order_id) -> Response:
        """
        Descripcion: Servicio que retorna una orden especifica que cumpla con los parametros especificados

        Parametros obligatorios (path params):
        "aggregator_name": nombre del agregador al cual pertenece la orden
        "store_id"       : identificador de la tienda, ruc + nro_establecimiento
        "order_id"       : identificador de la tienda

        Parametros opcionales (request params):
        """

        try:
            store = Store.objects.get(integrationId=store_id)
        except Exception as e:
            body = {'message': NOT_EXIST_STORE_ID.format(store_id), 'details': str(e)}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        try:
            aggregator = Aggregator.objects.get(name=aggregator_name)
        except Exception as e:
            body = {'message': AGGREGATOR_NOT_EXIST.format(aggregator_name), 'details': str(e)}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        if not store.active:
            body = {'message': NOT_ACTIVE_STORE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        orders = Order.objects.filter(order_id=order_id, store=store, aggregator=aggregator)

        if len(orders) == 1:
            order = orders[0]
            serializer = OrderSerializer(instance=order)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        if len(orders) == 0:
            body = {'message': NOT_EXIST_ORDER_BY_PARAMS, 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)
        else:
            body = {'message': MANY_EXIST_ORDER_BY_PARAMS, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False,
            url_path='status-final/(?P<aggregator_name>[^/.]+)/(?P<store_id>[^/.]+)/(?P<status_final>[^/.]+)')
    def get_orders_by_status_final(self, request, aggregator_name, store_id, status_final) -> Response:
        """
        Descripcion: Servicio que retorna un listado de ordenes filtradas por su estado final. Si no se especifican
        ninguno de los parametros opcioneles de la ruta "start" y "end" se retornan por defectos las que tengan el mismo
        dia, mes y ano que la fecha actual.

        Parametros obligatorios (path params):
        "aggregator_name": nombre del agregador al cual pertenece la orden
        "store_id"       : identificador de la tienda, ruc + nro_establecimiento
        "status_final"   : valor 0 o 1, donde 0 son las ordenes aceptadas y 1 las canceladas

        Parametros opcionales (request params):
        start: inicio del rango de tiempo para filtrar las ordenes por el atributo de la fecha de registro (created_at)
        en el API, si se especifica este parametro y no el parametro "end" devuelve entonces las ordenes activas con el
        mismo dia, mes y ano del parametro "start"

        end: fin del rango de tiempo para filtrar las ordenes por el atributo de la fecha de registro en el API.

        develop: si viene con valor se devuelven las ordenes buscadas en el ambiente de desarrollo del agregador, si no
        trae valor las del ambiente de produccion, si no se le especifica valor toma por defecto el ambiende de produccion
        """
        start = request.GET.get('start', None)
        end = request.GET.get('end', None)
        mode = ModeEnum.DEVELOP if request.GET.get('develop', False) else ModeEnum.PRODUCTION

        try:
            status_final = int(status_final)
        except Exception as e:
            body = {'message': "Parametro status_final invalido", 'details': str(e)}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        try:
            store = Store.objects.get(integrationId=store_id)
        except Exception as e:
            body = {'message': NOT_EXIST_STORE_ID.format(store_id), 'details': str(e)}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        try:
            aggregator = Aggregator.objects.get(name=aggregator_name)
        except Exception as e:
            body = {'message': AGGREGATOR_NOT_EXIST.format(aggregator_name), 'details': str(e)}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        if status_final != OrderStatusFinalEnum.SUCCESS.value and status_final != OrderStatusFinalEnum.CANCELED.value:
            body = {'message': UNKNOW_STATUS_FINAL, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        if not store.active:
            body = {'message': NOT_ACTIVE_STORE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        if mode == ModeEnum.DEVELOP and not store.develop_mode:
            body = {'message': NOT_ACTIVATE_DEVELOP_MODE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        if mode == ModeEnum.PRODUCTION and not store.production_mode:
            body = {'message': NOT_ACTIVATE_PRODUCTION_MODE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        try:
            datetime_start = dateutil.parser.isoparse(start) if start else None
            datetime_end = dateutil.parser.isoparse(end) if end else None
        except Exception as e:
            body = {'message': WRONG_DATE_FORMAT, 'details': str(e)}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        if aggregator_name in store.aggregator.values_list('name', flat=True):
            orders_list = []
            if not datetime_start or not datetime_end:
                now = datetime.now()
                orders = Order.objects.filter(created_at__year=now.year,
                                              created_at__month=now.month,
                                              created_at__day=now.day,
                                              aggregator=aggregator,
                                              store=store,
                                              status_final=status_final,
                                              mode=mode.value)
            elif datetime_start and datetime_end:
                orders = Order.objects.filter(created_at__range=[datetime_start, datetime_end],
                                              aggregator=aggregator,
                                              store=store,
                                              status_final=status_final,
                                              mode=mode.value)
            elif datetime_start:
                orders = Order.objects.filter(created_at__year=datetime_start.year,
                                              created_at__month=datetime_start.month,
                                              created_at__day=datetime_start.day,
                                              aggregator=aggregator,
                                              store=store,
                                              status_final=status_final,
                                              mode=mode.value)
            else:
                orders = []

            for order in orders:
                serializer = OrderSerializer(instance=order)
                orders_list.append(serializer.data)

            return Response(data=orders_list, status=status.HTTP_200_OK)

        else:
            message = NOT_RELATION_STORE_AGGREGATOR.format(aggregator_name)
            return Response(data={'message': message, 'details': ''}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, url_path='range/(?P<store_id>[^/.]+)')
    def get_orders_as_object_by_range_time(self, request, store_id):
        start = request.GET.get('start', None)
        end = request.GET.get('end', None)

        try:
            store = Store.objects.get(integrationId=store_id)
        except Exception as e:
            body = {'message': NOT_EXIST_STORE_ID.format(store_id), 'details': str(e)}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        try:
            datetime_start = dateutil.parser.isoparse(start) if start else None
            datetime_end = dateutil.parser.isoparse(end) if end else None
        except Exception as e:
            error_mssg = "Error en los formatos de las fechas"
            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                              exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

        orders = Order.objects.filter(created_at__range=[datetime_start, datetime_end], store_id=store.id_pk)
        serializer = OrderSerializer(instance=orders, many=True)

        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @action(detail=False, url_path='filter', url_name='filter_by_range')
    def get_orders_by_range_time(self, request):
        """
        Descripcion: Servicio que retorna un listado de ordenes filtradas esclusivamente por su fecha de creacion en el
        API. Si no se especifican ninguno de los dos parametros opcionales "start" y "end" devuelve las ordenes registradas
        en el API en los ultimos 7 dias.

        Parametros obligatorios (path params):

        Parametros opcionales (request params):
        start: inicio del rango de tiempo para filtrar las ordenes por el atributo de la fecha de registro en el API, si
        se especifica este parametro y no el parametro "end" devuelve entonces las ordenes cuya fecha de registro en el
        API es mayor a la del parametro "start"

        end: fin del rango de tiempo para filtrar las ordenes por el atributo de la fecha de registro en el API.
        """
        start = request.GET.get('start', None)
        end = request.GET.get('end', None)
        status_final = request.GET.get('statusFinal', None)

        try:
            datetime_start = dateutil.parser.isoparse(start) if start else None
            datetime_end = dateutil.parser.isoparse(end) if end else None
        except Exception as e:
            error_mssg = "Error en los formatos de las fechas"
            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                              exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

        if status_final:
            try:
                status_final = int(status_final)
            except Exception as e:
                error_mssg = "Formato del parametro del estado final de la orden incorrecto"
                return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                  exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

            if status_final not in [OrderStatusFinalEnum.SUCCESS.value,
                                    OrderStatusFinalEnum.CANCELED.value]:
                error_mssg = "Estado final de la orden desconocido"
                return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                  exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

        orders = []
        if datetime_start and datetime_end:
            if status_final:
                if status_final == OrderStatusFinalEnum.SUCCESS.value:
                    orders = Order.orders_success.filter(created_at__range=[datetime_start, datetime_end]).values(
                        'order_id',
                        'status',
                        'transaction_number',
                        'status_pixel',
                        'status_final',
                        'store__name',
                        'store__integrationId',
                        'aggregator',
                        'created_at',
                        'processed_at_pixel',
                        'aggregator__name',
                        'mode').order_by(
                        '-created_at')

                elif status_final and status_final == OrderStatusFinalEnum.CANCELED.value:
                    orders = Order.orders_canceled.filter(created_at__range=[datetime_start, datetime_end]).values(
                        'order_id',
                        'status',
                        'transaction_number',
                        'status_pixel',
                        'status_final',
                        'store__name',
                        'store__integrationId',
                        'aggregator',
                        'created_at',
                        'processed_at_pixel',
                        'aggregator__name',
                        'mode').order_by(
                        '-created_at')
            else:
                orders = Order.objects.filter(created_at__range=[datetime_start, datetime_end]).values('order_id',
                                                                                                       'status',
                                                                                                       'transaction_number',
                                                                                                       'status_pixel',
                                                                                                       'status_final',
                                                                                                       'store__name',
                                                                                                       'store__integrationId',
                                                                                                       'aggregator',
                                                                                                       'created_at',
                                                                                                       'processed_at_pixel',
                                                                                                       'aggregator__name',
                                                                                                       'mode').order_by(
                    '-created_at')
        elif datetime_start:
            if status_final:
                if status_final == OrderStatusFinalEnum.SUCCESS.value:
                    orders = Order.orders_success.filter(created_at__gte=datetime_start).values('order_id', 'status',
                                                                                                'transaction_number',
                                                                                                'status_pixel',
                                                                                                'status_final',
                                                                                                'store__name',
                                                                                                'store__integrationId',
                                                                                                'aggregator',
                                                                                                'created_at',
                                                                                                'processed_at_pixel',
                                                                                                'aggregator__name',
                                                                                                'mode').order_by(
                        '-created_at')
                elif status_final == OrderStatusFinalEnum.CANCELED.value:
                    orders = Order.orders_canceled.filter(created_at__gte=datetime_start).values('order_id', 'status',
                                                                                                 'transaction_number',
                                                                                                 'status_pixel',
                                                                                                 'status_final',
                                                                                                 'store__name',
                                                                                                 'store__integrationId',
                                                                                                 'aggregator',
                                                                                                 'created_at',
                                                                                                 'processed_at_pixel',
                                                                                                 'aggregator__name',
                                                                                                 'mode').order_by(
                        '-created_at')
            else:
                orders = Order.objects.filter(created_at__gte=datetime_start).values('order_id', 'status',
                                                                                     'transaction_number',
                                                                                     'status_pixel',
                                                                                     'status_final',
                                                                                     'store__name',
                                                                                     'store__integrationId',
                                                                                     'aggregator',
                                                                                     'created_at',
                                                                                     'processed_at_pixel',
                                                                                     'aggregator__name',
                                                                                     'mode').order_by(
                    '-created_at')

        else:
            last_day = datetime.now() - timedelta(days=7)
            if status_final:
                if status_final == OrderStatusFinalEnum.SUCCESS.value:
                    orders = Order.orders_success.filter(created_at__gte=last_day).values('order_id',
                                                                                          'status',
                                                                                          'transaction_number',
                                                                                          'status_pixel',
                                                                                          'status_final',
                                                                                          'store__name',
                                                                                          'store__integrationId',
                                                                                          'aggregator',
                                                                                          'created_at',
                                                                                          'processed_at_pixel',
                                                                                          'aggregator__name',
                                                                                          'mode').order_by(
                        '-created_at')

                elif status_final == OrderStatusFinalEnum.CANCELED.value:
                    orders = Order.orders_canceled.filter(created_at__gte=last_day).values('order_id',
                                                                                           'status',
                                                                                           'transaction_number',
                                                                                           'status_pixel',
                                                                                           'status_final',
                                                                                           'store__name',
                                                                                           'store__integrationId',
                                                                                           'aggregator',
                                                                                           'created_at',
                                                                                           'processed_at_pixel',
                                                                                           'aggregator__name',
                                                                                           'mode').order_by(
                        '-created_at')
            else:
                orders = Order.objects.filter(created_at__gte=last_day).values('order_id',
                                                                               'status',
                                                                               'transaction_number',
                                                                               'status_pixel',
                                                                               'status_final',
                                                                               'store__name',
                                                                               'store__integrationId',
                                                                               'aggregator',
                                                                               'created_at',
                                                                               'processed_at_pixel',
                                                                               'aggregator__name',
                                                                               'mode').order_by(
                    '-created_at')

        for order in orders:
            order.update({'mode': dict_mode.get(order['mode'])})
            order.update({'status': dict_status_rappi.get(order['status'])})
            order.update({'status_pixel': dict_status_pixel.get(order['status_pixel'])})
            order.update({'status_final': dict_status_final.get(order['status_final'])})
            order.update({'created_at': order['created_at'].strftime("%d/%m/%Y %I:%M %p")})
            if order['processed_at_pixel']:
                order.update({'processed_at_pixel': order['processed_at_pixel'].strftime("%d/%m/%Y %I:%M:%S %p")})

        return Response(orders, status=status.HTTP_200_OK)

    @action(detail=False, methods=['put'],
            url_path='update-status/(?P<aggregator_name>[^/.]+)/(?P<store_id>[^/.]+)/(?P<order_id>[^/.]+)/(?P<status_to_update>[^/.]+)')
    def update_status_aggregator(self, request, aggregator_name, store_id, order_id, status_to_update) -> Response:
        """
        Descripcion: Servicio que permite actualizar el estado de una orden en el agregador Rappi, implementado solo
        para los estados TAKEN, REJECTED, y READY_FOR_PICKUP.

        Parametros obligatorios (path params):
        "aggregator_name": nombre del agregador al cual pertenece la orden
        "store_id"       : identificador de la tienda, ruc + nro_establecimiento
        "order_id"       : identificador de la tienda
        "status"         : valor numerico solo validos cuando el agregador donde va a actualizar la orden es Rappi, puede
        tomar los valores 3, 4 y 6, donde 3 es si desea actualizar la orden como taken (aceptada), 4 como rejected
        (rechazada) y 6 como ready_for_pickup.

        Parametros opcionales (request params):
        cookingTime: tiempo de coccion de la orden

        transactionNumber: numero de transaccion de la aceptacion de la orden

        dateProcessed: fecha de procesamiento de la orden en UTC, si no se especifica se toma por defecto la fecha actual

        develop: si viene con valor realiza la actualizacion de la orden en el ambiende de desarrollo del agregador, si
        no trae valor en el ambiende de produccion, si no se define toma por defecto el ambiente de produccion

        Implementado para los agregadores:
        - Rappi
        """
        mode = ModeEnum.DEVELOP if request.GET.get('develop', False) else ModeEnum.PRODUCTION
        cooking_time = request.GET.get('cookingTime', None)
        transaction_number = request.GET.get('transactionNumber', None)
        date_processed = request.GET.get('dateProcessed', None)
        trace_type = TraceTypeEnum.ORDER_CHANGE_STATUS

        try:
            status_to_update = int(status_to_update)
        except Exception as e:
            insert_trace(trace_type=trace_type.value,
                         instance_id=status,
                         exception=str(e),
                         mode=mode.value)
            body = {'message': 'Parametro status incorrecto', 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        store = self.store_ctl.get_store_by_store_id(store_id, trace_type, mode)
        if not store:
            body = {'message': NOT_EXIST_STORE_ID.format(store_id), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        aggregator = self.aggregator_ctl.get_aggregator_by_name(aggregator_name, trace_type, mode)
        if not aggregator:
            body = {'message': AGGREGATOR_NOT_EXIST.format(aggregator_name), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        if not self.store_ctl.store_define_active(store, aggregator, trace_type, mode):
            body = {'message': NOT_ACTIVE_STORE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        selected_mode_active = self.store_ctl.store_define_mode_active(store, aggregator, trace_type, mode)
        if not selected_mode_active:
            body = {'message': STORE_NOT_ACTIVE_MODE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = StoreAggregator.objects.filter(store=store, aggregator=aggregator)
        if not store_aggregator:
            body = {'message': STORE_NOT_RELATED, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = self.store_ctl.get_store_aggregator_relation(store, aggregator, trace_type, mode)

        base_url = self.aggregator_ctl.get_base_url_by_mode(mode, aggregator, store_id, trace_type)
        if not base_url:
            body = {'message': STORE_NOT_URL_BASE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        orders = Order.objects.filter(order_id=order_id, store=store, aggregator=aggregator)

        if len(orders) == 1:
            order = orders[0]
            headers = get_headers_by_aggregator(aggregator_name, store, mode, store_aggregator)
            if aggregator_name == AggregatorEnum.RAPPI.value:
                return self.update_order_status_in_rappi(aggregator_name, base_url, cooking_time, date_processed, mode,
                                                         order, request.data, transaction_number, trace_type, headers,
                                                         status_to_update)

            elif aggregator_name == AggregatorEnum.UBBER.value:
                return Response(data={'message': 'Aun no implementado', 'details': ''},
                                status=status.HTTP_400_BAD_REQUEST)

            elif aggregator_name == AggregatorEnum.GLOVO.value:
                return Response(data={'message': 'Aun no implementado', 'details': ''},
                                status=status.HTTP_400_BAD_REQUEST)

            else:
                return Response(data={'message': 'Agregador desconocido', 'details': ''},
                                status=status.HTTP_400_BAD_REQUEST)

        if len(orders) == 0:
            body = {'message': NOT_EXIST_ORDER_BY_PARAMS, 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)
        else:
            body = {'message': MANY_EXIST_ORDER_BY_PARAMS, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, url_path='events/(?P<aggregator_name>[^/.]+)/(?P<store_id>[^/.]+)/(?P<order_id>[^/.]+)')
    def get_order_events(self, request, aggregator_name, store_id, order_id):
        """
        Descripcion: Servicio que permite obtener el listado de eventos de una orden en su agregador.

        Parametros obligatorios (path params):
        "aggregator_name": nombre del agregador al cual pertenece la orden
        "store_id"       : identificador de la tienda, ruc + nro_establecimiento
        "order_id"       : identificador de la orden

        Parametros opcionales (request params):
        develop: si viene con valor define si se actualiza la orden como aceptada en el ambiente de desarrollo o
        produccion del agregador, por defecto toma el ambiente de produccion

        Implementado para los agregadores:
        - Rappi
        """
        mode = ModeEnum.DEVELOP if request.GET.get('develop', False) else ModeEnum.PRODUCTION

        trace_type = TraceTypeEnum.ORDER_EVENTS
        store = self.store_ctl.get_store_by_store_id(store_id, trace_type, mode)
        if not store:
            body = {'message': NOT_EXIST_STORE_ID.format(store_id), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        aggregator = self.aggregator_ctl.get_aggregator_by_name(aggregator_name, trace_type, mode)
        if not aggregator:
            body = {'message': AGGREGATOR_NOT_EXIST.format(aggregator_name), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        if not self.store_ctl.store_define_active(store, aggregator, trace_type, mode):
            body = {'message': NOT_ACTIVE_STORE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        selected_mode_active = self.store_ctl.store_define_mode_active(store, aggregator, trace_type, mode)
        if not selected_mode_active:
            body = {'message': STORE_NOT_ACTIVE_MODE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = StoreAggregator.objects.filter(store=store, aggregator=aggregator)
        if not store_aggregator:
            body = {'message': STORE_NOT_RELATED, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = self.store_ctl.get_store_aggregator_relation(store, aggregator, trace_type, mode)

        base_url = self.aggregator_ctl.get_base_url_by_mode(mode, aggregator, store_id, trace_type)
        if not base_url:
            body = {'message': STORE_NOT_URL_BASE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        orders = Order.objects.filter(order_id=order_id, store=store, aggregator=aggregator)

        if len(orders) == 1:
            order = orders[0]
            headers = get_headers_by_aggregator(aggregator_name, store, mode, store_aggregator)
            if aggregator_name == AggregatorEnum.RAPPI.value:
                url = base_url + '/orders/{}/events'.format(order_id)
                response = requests.get(url=url, headers=headers)

                if response.status_code == status.HTTP_200_OK:
                    body = json.loads(response.content)
                    return Response(data=body, status=status.HTTP_200_OK)
                else:
                    exception = "It was not possible to get the events of the Order with id {} because bad status response received from Aggregator {}, details: {}".format(
                        order_id, aggregator_name, str(response.content))
                    insert_trace(trace_type=trace_type.value,
                                 instance_id=order_id,
                                 exception=exception,
                                 mode=mode.value)

                    body = {'message': NOT_GET_OREDER_EVENTS, 'details': json.loads(response.content)}
                    return Response(data=body, status=response.status_code)

            elif aggregator_name == AggregatorEnum.UBBER.value:
                return Response(data={'message': 'Aun no implementado', 'details': ''},
                                status=status.HTTP_400_BAD_REQUEST)
            elif aggregator_name == AggregatorEnum.GLOVO.value:
                return Response(data={'message': 'Aun no implementado', 'details': ''},
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(data={'message': 'Agregador desconocido', 'details': ''},
                                status=status.HTTP_400_BAD_REQUEST)

        if len(orders) == 0:
            body = {'message': NOT_EXIST_ORDER_BY_PARAMS, 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)
        else:
            body = {'message': MANY_EXIST_ORDER_BY_PARAMS, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, url_path='fake/orders')
    def get_orders_fake(self, request):
        """
        Servicio que retorna un listado de ordenes generadas de forma aleatoria
        """
        orders_list = []
        i = randint(1, 5)
        if i % 3 == 0:
            orders = [
                {
                    "store": {
                        "name": "Papa John's - PIXEL",
                        "external_id": "8246",
                        "internal_id": "8246"
                    },
                    "customer": {
                        "email": "client@gmail.com",
                        "last_name": "SAS",
                        "first_name": "Rappi",
                        "phone_number": "+57 031 3163535"
                    },
                    "order_detail": {
                        "items": [
                            {
                                "id": "2136205722",
                                "sku": "2141000160",
                                "name": "Orig 8\" All The Meats",
                                "type": "product",
                                "comments": None,
                                "quantity": 1,
                                "subitems": [],
                                "percentage_discount": 10,
                                "unit_price_with_discount": 6.36,
                                "unit_price_without_discount": 7.07
                            }
                        ],
                        "totals": {
                            "charges": {
                                "shipping": 4.5,
                                "service_fee": 4.5
                            },
                            "total_order": 15.36,
                            "other_totals": {
                                "tip": 0,
                                "total_rappi_pay": 0,
                                "total_rappi_credits": 0
                            },
                            "total_to_pay": 15.36,
                            "total_other_discounts": 0,
                            "total_products_with_discount": 6.36,
                            "total_products_without_discount": 7.07
                        },
                        "order_id": str(randint(1, 8952014)),
                        "created_at": str(datetime.now()),
                        "cooking_time": 15,
                        "payment_method": "cash",
                        "delivery_method": "marketplace",
                        "max_cooking_time": 20,
                        "min_cooking_time": 10,
                        "delivery_discount": {
                            "total_value_discount": 0,
                            "total_percentage_discount": 0
                        },
                        "billing_information": {
                            "name": "client",
                            "email": "client@gmail.com",
                            "phone": "43333222",
                            "address": "Some street 12345",
                            "billing_type": "Factura",
                            "document_type": "DNI",
                            "document_number": "32432342"
                        },
                        "delivery_information": {
                            "city": "Narnia",
                            "complement": "",
                            "description": "dtpo 1001",
                            "postal_code": "111061",
                            "neighborhood": "El Laurel",
                            "complete_address": "Calle 68 B Bis N 70c - 12"
                        }
                    }
                }
            ]
            orders_list.extend(orders)
        return Response(orders_list, status=status.HTTP_200_OK)

    # @action(detail=False, methods=['post'],
    #         url_path='served/(?P<store_id>[^/.]+)/(?P<order_id>[^/.]+)')
    # def mark_as_served(self, request: Request, store_id, order_id) -> Response:
    #     """
    #     Allows update an order as served
    #     """
    #     try:
    #         store = Store.objects.get(integrationId=store_id)
    #     except:
    #         return Response({'message': 'Store with id {} do not exist'.format(store_id)},
    #                         status=status.HTTP_404_NOT_FOUND)
    #
    #     try:
    #         Order.objects.get(order_id=order_id)
    #     except:
    #         return Response({'message': 'Order with id {} do not exist'.format(order_id)},
    #                         status=status.HTTP_404_NOT_FOUND)
    #
    #     if AggregatorEnum.GLOVO.value in store.aggregator.values_list('name', flat=True):
    #         url = settings.API_GLOVO_PRODUCTION_URL + '/webhook/stores/{}/orders/{}/serve'.format(store_id, order_id)
    #         response = requests.post(url, data={}, headers={'Authorization': 'miToken'})
    #         if response.status_code == status.HTTP_200_OK:
    #             return Response(data={'message': 'Order marked as served successfully'}, status=status.HTTP_200_OK)
    #
    #         return Response(data=json.loads(response.content), status=response.status_code)
    #
    #     else:
    #         return Response(data={'message': 'Store with id {} is not related to the {} aggregator'.format(store_id,
    #                                                                                                        settings.AGGREGATORS[
    #                                                                                                            0])},
    #                         status=status.HTTP_400_BAD_REQUEST)

    def update_order_status_in_rappi(self, aggregator_name, base_url, cooking_time, date_processed, mode, order, data,
                                     transaction_number, trace_type, headers, status_to_update):
        if status_to_update == OrderStatusRappiEnum.TAKEN.value:
            if not transaction_number:
                exception = "Order with id {} not updated as TAKEN because not received a value in the param \"transactionNumber\"".format(
                    order.order_id)
                insert_trace(trace_type=trace_type.value,
                             instance_id=order.order_id,
                             exception=exception,
                             mode=mode.value)
                body = {'message': ORDER_WITHOUT_TRANSACTION_NUMBER, 'details': ''}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            if not date_processed:
                date_processed_datetime = datetime.now()
            else:
                try:
                    date_processed_datetime = datetime.strptime(date_processed, '%Y-%m-%dT%H:%M:%SZ')
                except Exception as e:
                    exception = "Order with id {} not updated as TAKEN because param \"dateProcessed\" with value {} have wrong format, details:{}".format(
                        order.order_id, date_processed, e)
                    insert_trace(trace_type=trace_type.value,
                                 instance_id=order.order_id,
                                 exception=exception,
                                 mode=mode.value)
                    body = {'message': WRONG_DATE_FORMAT, 'details': str(e)}
                    return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            try:
                order.status_pixel = OrderStatusPixelEnum.PROCESSED.value
                order.processed_at = datetime.now()
                order.processed_at_pixel = date_processed_datetime
                order.transaction_number = transaction_number
                order.save()
            except Exception as e:
                exception = "Order with id {} not updated as TAKEN in Aggregator {} because error updating params \"status_pixel\",\"processed_at\",\"processed_at_pixel\",\"transaction_number\", details: {}".format(
                    order.order_id, aggregator_name, e)
                insert_trace(trace_type=trace_type.value,
                             instance_id=order.order_id,
                             exception=exception,
                             mode=mode.value)
                body = {'message': ERROR_UPDATE_ORDER, 'details': str(e)}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            url = base_url + '/orders/{}/take'.format(order.order_id)

            if cooking_time:
                url = base_url + '/orders/{}/take/{}'.format(order.order_id, cooking_time)

            try:
                response = requests.put(url=url, headers=headers, data=json.dumps(data))
            except Exception as e:
                exception = "Order with id {} not updated as TAKEN, details: {}".format(order.order_id, e)
                insert_trace(trace_type=trace_type.value,
                             instance_id=order.order_id,
                             exception=exception,
                             mode=mode.value)
                body = {'message': ORDER_NOT_UPDATED_STATUS_TAKEN, 'details': ''}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            if response.status_code == status.HTTP_200_OK:
                try:
                    order.status_final = OrderStatusFinalEnum.SUCCESS.value
                    order.status = OrderStatusRappiEnum.TAKEN.value
                    order.cooking_time = cooking_time
                    order.save()
                except Exception as e:
                    exception = "Order with id {} not updated prams \"status\",\"status_final\",\"cooking_time\", details: {}".format(
                        order.order_id, e)
                    insert_trace(trace_type=trace_type.value,
                                 instance_id=order.order_id,
                                 exception=exception,
                                 mode=mode.value)
                    body = {'message': ERROR_UPDATE_ORDER, 'details': str(e)}
                    return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

                exception = "Order with id {} updated as TAKEN success in Aggregator {}".format(order.order_id,
                                                                                                aggregator_name)
                insert_trace(trace_type=trace_type.value,
                             instance_id=order.order_id,
                             exception=exception,
                             mode=mode.value)

                return Response(data={'message': ORDER_UPDATE_STATUS_TAKEN}, status=response.status_code)

            else:
                exception = "Order with id {} not updated as TAKEN because bad status response received from Aggregator {}, details: {}".format(
                    order.order_id, aggregator_name, str(response.content))
                insert_trace(trace_type=trace_type.value,
                             instance_id=order.order_id,
                             exception=exception,
                             mode=mode.value)
                body = {'message': ORDER_NOT_UPDATED_STATUS_TAKEN, 'details': str(response.content)}
                return Response(data=body, status=response.status_code)

        elif status_to_update == OrderStatusRappiEnum.REJECTED.value:
            try:
                order.status_final = OrderStatusFinalEnum.CANCELED.value
                order.status_pixel = OrderStatusPixelEnum.REJECTED.value
                order.save()
            except Exception as e:
                exception = "Order with id {} not updated as REJECTED because can not updated prams \"status_pixel\",\"status_final\", details: {}".format(
                    order.order_id, e)
                insert_trace(trace_type=trace_type.value,
                             instance_id=order.order_id,
                             exception=exception,
                             mode=mode.value)
                body = {'message': ERROR_UPDATE_ORDER, 'details': ''}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            if not OrderCanceled.objects.filter(order=order):
                try:
                    order_canceled = OrderCanceled(order=order, cancel_type=OrderCancelTypeEnum.USER.value)
                    order_canceled.save()
                except Exception as e:
                    exception = 'Order with id {} not updated as REJECTED because error inserting a OrderCanceled instance, details: {}'.format(
                        order.order_id, e)
                    insert_trace(trace_type=trace_type.value,
                                 instance_id=order.order_id,
                                 exception=exception,
                                 mode=mode.value)
                    body = {'message': ERROR_UPDATE_ORDER, 'details': str(e)}
                    return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            url = base_url + '/orders/{}/reject'.format(order.order_id)

            try:
                response = requests.put(url=url, data=json.dumps(data), headers=headers)
            except Exception as e:
                print(str(e))
                exception = "Order with id {} not updated as REJECTED, details: {}".format(order.order_id, e)
                insert_trace(trace_type=trace_type.value,
                             instance_id=order.order_id,
                             exception=exception,
                             mode=mode.value)
                body = {'message': ORDER_NOT_UPDATED_STATUS_REJECTED, 'details': ''}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            if response.status_code == status.HTTP_200_OK:
                try:
                    order.status = OrderStatusRappiEnum.REJECTED.value
                    order.save()
                except Exception as e:
                    exception = "Order with id {} not updated prams \"status\", details: {}".format(order.order_id, e)
                    insert_trace(trace_type=trace_type.value,
                                 instance_id=order.order_id,
                                 exception=exception,
                                 mode=mode.value)
                    body = {'message': ERROR_UPDATE_ORDER, 'details': str(e)}
                    return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

                exception = "Order with id {} updated as REJECTED success in Aggregator {}".format(order.order_id,
                                                                                                   aggregator_name)
                insert_trace(trace_type=trace_type.value,
                             instance_id=order.order_id,
                             exception=exception,
                             mode=mode.value)

                return Response(data={'message': ORDER_UPDATE_STATUS_REJECTED}, status=response.status_code)

            else:
                exception = "Order with id {} not updated as REJECTED because bad status response received from Aggregator {}, details: {}".format(
                    order.order_id, aggregator_name, str(response.content))
                insert_trace(trace_type=trace_type.value,
                             instance_id=order.order_id,
                             exception=exception,
                             mode=mode.value)
                body = {'message': ERROR_UPDATE_ORDER, 'details': str(response.content)}
                return Response(data=body, status=response.status_code)

        elif status_to_update == OrderStatusRappiEnum.READY_FOR_PICKUP.value:
            url = base_url + '/orders/{}/ready-for-pickup'.format(order.order_id)
            response = requests.post(url=url, data=data, headers=headers)

            if response.status_code == status.HTTP_200_OK:
                order.status = OrderStatusRappiEnum.READY_FOR_PICKUP.value
                order.save()

                body = {'message': ORDER_UPDATED_STATUS_READY_PICK, 'details': ''}
                return Response(data=body, status=response.status_code)
            else:
                exception = "Order with id {} not updated as ready-for-pickup because bad status response received from Aggregator {}, details: {}".format(
                    order.order_id, aggregator_name, str(response.content))
                insert_trace(trace_type=trace_type.value,
                             instance_id=order.order_id,
                             exception=exception,
                             mode=mode.value)

                body = {'message': ORDER_NOT_UPDATED_STATUS_READY_PICK, 'details': json.loads(response.content)}
                return Response(data=body, status=response.status_code)
        else:
            body = {'message': 'No implementado', 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

    def insert_order_not_inserted(self, order_id=None, store_id=None, aggregator_name=None, order_json=None,
                                  exception=None, mode=None):
        return OrderNotReceived.objects.create(**{'order_id': order_id,
                                                  'store_id': store_id,
                                                  'aggregator_name': aggregator_name,
                                                  'order_json': order_json,
                                                  'exception': exception,
                                                  'mode': mode})
