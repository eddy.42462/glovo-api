from rest_framework import viewsets, status
from rest_framework.response import Response

from ..models import Order, OrderCancelation
from ..serializers.order_cancelation_serializer import OrderCancelationSerializer


class OrderCancelationViewSet(viewsets.ModelViewSet):
    """
    retrieve:
        Return a OrderCancelation instance.

    list:
        Return all OrderCancelations, ordered by order_id.

    create:
        Create a new OrderCancelation.

    delete:
        Remove an existing OrderCancelation.

    partial_update:
        Update one or more fields on an existing OrderCancelation.

    update:
        Update a OrderCancelation.
    """
    queryset = OrderCancelation.objects.all().order_by('order_id')
    serializer_class = OrderCancelationSerializer

    def create(self, request, **kwargs):
        # order_id = request.data.get('order_id', None)
        # try:
        #     order = Order.objects.get(order_id=order_id)
        # except:
        #     message = 'No existe una orden con identificador {}'.format(order_id)
        #     return Response(data={'message': message}, status=status.HTTP_400_BAD_REQUEST)
        #
        # if OrderCancelation.objects.filter(order_id=order_id).exists():
        #     message = 'Orden con identificador {} ya fue cancelada'.format(order_id)
        #     return Response(data={'message': message}, status=status.HTTP_400_BAD_REQUEST)
        #
        # serializer_data = {
        #     'order_id': order.order_id,
        #     'cancel_reason': request.data.get('cancel_reason', None),
        #     'payment_strategy': request.data.get('payment_strategy', None)
        # }
        #
        # serializer = OrderCancelationSerializer(data=serializer_data)
        #
        # if not serializer.is_valid():
        #     message = 'No pudo ser cancelada la orden con identificador {}'.format(order_id)
        #     return Response(data={'message': message}, status=status.HTTP_400_BAD_REQUEST)
        #
        # order_canceled = serializer.save()
        #
        # if order_canceled:
        #     return Response(data={'message': 'Orden con cancelada satisfactoriamente'}, status=status.HTTP_201_CREATED)
        # return Response(data={'message': 'No pudo ser cancelada la orden'}, status=status.HTTP_400_BAD_REQUEST)
        pass
