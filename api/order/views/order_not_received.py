from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from store.dicts import dict_status_rappi, dict_status_pixel, dict_status_final, dict_mode

from ..models import OrderNotReceived
from ..serializers.order_not_received_serializer import OrderNotReceivedSerializer
from store.models import Store, Aggregator


class OrderNotReceivedViewSet(viewsets.ModelViewSet):
    """
    Servicios de OrderNotReceived
    """
    queryset = OrderNotReceived.objects.all().order_by('order_id')
    serializer_class = OrderNotReceivedSerializer

    @action(detail=False, url_path='filter', url_name='orders_not_received')
    def get_orders_by_range_time(self, request):
        """

        """
        orders_not_received = OrderNotReceived.objects.values('order_id',
                                                              'store_id',
                                                              'aggregator_id',
                                                              'mode',
                                                              'exception',
                                                              'created_at')

        for order in orders_not_received:
            order.update({'mode': dict_mode.get(order['mode'])})
            store = Store.objects.get(id_pk=order['store_id'])
            aggregater = Aggregator.objects.get(id_pk=order['aggregator_id'])
            order.update({'store_id': store.integrationId})
            order.update({'aggregator_id': aggregater.name})
            order.update({'created_at': order['created_at'].strftime("%d/%m/%Y %I:%M %p")})

        return Response(orders_not_received, status=status.HTTP_200_OK)
