import re
from rest_framework import permissions
from rest_framework.exceptions import ValidationError
from django.conf import settings



def validate_date_format(date_string):
    exp = '[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]'
    if not re.match(exp, date_string):
        message = '{}  not have correct date format yyyy-MM-dd HH:mm:ss'.format(date_string)
        raise ValidationError(message)


class OriginHostPermission(permissions.BasePermission):
    """
    Host permission check.
    """
    expects_authentication = False
    message = 'Origin host not allowed.'

    def has_permission(self, request, view):
        host = request.headers.get('Host', None)
        return host in settings.LIST_HOST_ALLOW_CANCEL_ORDER
