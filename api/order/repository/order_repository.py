from ..models import Order


class OrderRepository:

    def get_orders_by_status_final(self, status_final):
        return Order.objects.filter(status_final=status_final)
