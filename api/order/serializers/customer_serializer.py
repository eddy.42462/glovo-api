from rest_framework import serializers

from .invoice_serializer import InvoiceSerializer
from ..models import Customer


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model Customer
    """
    invoicings = InvoiceSerializer(many=True, read_only=True)

    class Meta:
        model = Customer
        fields = ('name',
                  'phone_number',
                  'hash',
                  'first_name',
                  'last_name',
                  'user_type',
                  'email',
                  'document_number',
                  'invoicings')
