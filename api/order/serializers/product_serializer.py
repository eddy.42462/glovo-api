from django.db import IntegrityError
from jsonschema import ValidationError
from rest_framework import serializers

from ..models import Order, ProductOrder
from .order_serializer import OrderSerializer


class BulkCreateUpdateProductListSerializer(serializers.ListSerializer):

    def create(self, validated_data):
        result = [self.child.create(attrs) for attrs in validated_data]
        try:
            self.child.Meta.model.objects.bulk_create(result, ignore_conflicts=True)
        except IntegrityError as e:
            raise ValidationError(e)
        return result

    def update(self, instances, validated_data):
        instance_hash = {index: instance for index, instance in enumerate(instances)}
        result = [
            self.child.update(instance_hash[index], attrs)
            for index, attrs in enumerate(validated_data)
        ]
        writable_fields = [
            x
            for x in self.child.Meta.fields
            if x not in self.child.Meta.read_only_fields
        ]
        try:
            self.child.Meta.model.objects.bulk_update(result, writable_fields)
        except IntegrityError as e:
            raise ValidationError(e)
        return result


class ProductOrderSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model ProductOrder
    """
    order = OrderSerializer(read_only=True)
    order_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Order.objects.all(), source="order")

    class Meta:
        model = ProductOrder
        fields = ('id',
                  'name',
                  'price',
                  'quantity',
                  'purchased_product_id',
                  'parent_item_id',
                  'sku',
                  'type',
                  'comments',
                  'unit_price_with_discount',
                  'unit_price_without_discount',
                  'percentage_discount',
                  'order_id',
                  'order')
        list_serializer_class = BulkCreateUpdateProductListSerializer
