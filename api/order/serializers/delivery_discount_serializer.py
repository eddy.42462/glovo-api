from rest_framework import serializers

from ..models import DeliveryDiscount


class DeliveryDiscountSerializer(serializers.HyperlinkedModelSerializer):
    """
        Serializer of the model DeliveryDiscount
    """

    class Meta:
        model = DeliveryDiscount
        fields = ('total_percentage_discount',
                  'total_value_discount')
