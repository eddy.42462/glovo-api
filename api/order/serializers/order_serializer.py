from rest_framework import serializers
from store.serializers import AggregatorSerializer, Aggregator, Store, StoreSerializer

from .courier_serializer import CourierSerializer
from .customer_serializer import CustomerSerializer
from .delivery_discount_serializer import DeliveryDiscountSerializer
from .delivery_information_serializer import DeliveryInformationSerializer
from .billing_information_serializer import BillingInformationSerializer
from .order_canceled_serializer import OrderCanceledSerializer
from ..models import Order, ProductOrder, Customer, DeliveryInformation, Courier, BillingInformation, DeliveryDiscount


class ProductOrderSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model ProductOrder
    """
    order = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = ProductOrder
        fields = ('id',
                  'name',
                  'price',
                  'quantity',
                  'purchased_product_id',
                  'parent_item_id',
                  'sku',
                  'type',
                  'comments',
                  'unit_price_with_discount',
                  'unit_price_without_discount',
                  'percentage_discount',
                  'order')


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model Order
    """
    order_json = serializers.JSONField(write_only=True)
    store = StoreSerializer(read_only=True)
    store_id = serializers.PrimaryKeyRelatedField(write_only=True,
                                                  queryset=Store.objects.all(),
                                                  source='store')
    aggregator = AggregatorSerializer(read_only=True)
    aggregator_id = serializers.PrimaryKeyRelatedField(write_only=True,
                                                       queryset=Aggregator.objects.all(),
                                                       source='aggregator')
    courier = CourierSerializer(read_only=True)
    courier_id = serializers.PrimaryKeyRelatedField(write_only=True,
                                                    queryset=Courier.objects.all(),
                                                    source='courier',
                                                    required=False,
                                                    allow_null=True)
    customer = CustomerSerializer(read_only=True)
    customer_id = serializers.PrimaryKeyRelatedField(write_only=True,
                                                     queryset=Customer.objects.all(),
                                                     source='customer',
                                                     required=False,
                                                     allow_null=True)
    delivery_information = DeliveryInformationSerializer(read_only=True)
    delivery_information_id = serializers.PrimaryKeyRelatedField(write_only=True,
                                                                 queryset=DeliveryInformation.objects.all(),
                                                                 source='delivery_information',
                                                                 required=False,
                                                                 allow_null=True)
    billing_information = BillingInformationSerializer(read_only=True)
    billing_information_id = serializers.PrimaryKeyRelatedField(write_only=True,
                                                                queryset=BillingInformation.objects.all(),
                                                                source='billing_information',
                                                                required=False,
                                                                allow_null=True)
    delivery_discount = DeliveryDiscountSerializer(read_only=True)
    delivery_discount_id = serializers.PrimaryKeyRelatedField(write_only=True,
                                                              queryset=DeliveryDiscount.objects.all(),
                                                              source='delivery_discount',
                                                              required=False,
                                                              allow_null=True)
    products = ProductOrderSerializer(many=True, read_only=True)
    order_canceled = OrderCanceledSerializer(read_only=True)

    class Meta:
        model = Order
        fields = ('order_id', 'order_json', 'store_id', 'store', 'order_time', 'estimated_pickup_time',
                  'utc_offset_minutes', 'payment_method', 'currency', 'order_code', 'allergy_info',
                  'total_rappi_credits', 'total_to_pay', 'delivery_discount', 'delivery_discount_id',
                  'estimated_total_price', 'delivery_fee', 'minimum_basket_surcharge', 'customer_cash_payment_amount',
                  'aggregator', 'aggregator_id', 'courier', 'courier_id', 'customer', 'customer_id', 'products',
                  'delivery_information', 'delivery_information_id', 'bundled_orders', 'pick_up_code',
                  'is_picked_up_by_customer', 'cutlery_requested', 'total_customer_to_pay', 'cooking_time',
                  'min_cooking_time', 'tip', 'total_rappi_pay', 'max_cooking_time', 'created_at_rappi',
                  'delivery_method', 'status', 'billing_information', 'billing_information_id', 'status_pixel',
                  'total_products_with_discount', 'total_products_without_discount', 'total_other_discounts',
                  'total_order', 'shipping', 'service_fee', 'mode', 'order_canceled', 'status_final')

