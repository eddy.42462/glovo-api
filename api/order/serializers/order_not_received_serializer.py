from rest_framework import serializers

from ..models import OrderNotReceived


class OrderNotReceivedSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model OrderNotReceived
    """

    class Meta:
        model = OrderNotReceived
        fields = ('id',
                  'order_id',
                  'store_id',
                  'aggregator_id',
                  'exception',
                  'mode',
                  'created_at')
