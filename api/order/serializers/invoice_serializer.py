from rest_framework import serializers

from ..models import Invoice


class InvoiceSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model Invoice
    """

    class Meta:
        model = Invoice
        fields = ('company_name',
                  'company_address',
                  'tax_id')
