from rest_framework import serializers

from ..models import OrderCanceled


class OrderCanceledSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model OrderCanceled
    """

    class Meta:
        model = OrderCanceled
        fields = ('cancel_type', 'created_at')
