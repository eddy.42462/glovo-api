from rest_framework import serializers

from ..models import Courier


class CourierSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model Courier
    """

    class Meta:
        model = Courier
        fields = ('name', 'phone_number')
