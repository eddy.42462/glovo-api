from rest_framework import serializers

from ..models import DeliveryInformation


class DeliveryInformationSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model DeliveryInformation
    """

    class Meta:
        model = DeliveryInformation
        fields = ('label',
                  'longitude',
                  'latitude',
                  'city',
                  'complete_address',
                  'description',
                  'neighborhood',
                  'postal_code',
                  'complement')
