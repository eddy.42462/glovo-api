from rest_framework import serializers

from ..models import BillingInformation


class BillingInformationSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model BillingInformation
    """

    class Meta:
        model = BillingInformation
        fields = ('billing_type',
                  'name',
                  'address',
                  'phone',
                  'email',
                  'document_type',
                  'document_number')
