from rest_framework import serializers

from ..models import OrderCancelation, Order


class OrderCancelationSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model OrderCancelation
    """
    order_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=Order.objects.all(), source='order')

    class Meta:
        model = OrderCancelation
        fields = ('order_id',
                  'cancel_reason',
                  'payment_strategy')
