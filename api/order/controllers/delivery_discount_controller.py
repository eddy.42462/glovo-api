from ..serializers.delivery_discount_serializer import DeliveryDiscountSerializer


class DeliveryDiscountController:

    def insert_delivery_discount_rappi(self, data):
        dalivery_discount_serializer = DeliveryDiscountSerializer(data=data)
        if dalivery_discount_serializer.is_valid():
            return dalivery_discount_serializer.save()
        return None
