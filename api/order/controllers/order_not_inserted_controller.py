from ..models import OrderNotReceived


class OrderNotReceivedController:

    def insert_order_not_inserted(self, mode, order_id=None, store_id=None, aggregator_id=None, order_json=None,
                                  exception=None):
        return OrderNotReceived.objects.create(**{'order_id': order_id,
                                                  'store_id': store_id,
                                                  'aggregator_id': aggregator_id,
                                                  'order_json': order_json,
                                                  'exception': exception,
                                                  'mode': mode})
