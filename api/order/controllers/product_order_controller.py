from ..serializers.product_serializer import ProductOrderSerializer


class ProductOrderController:

    def insert_product_rappi(self, order, products_data):
        products_to_insert = []
        for product_data in products_data:
            new_products_to_insert = self.select_all_products_datas(order=order,
                                                                    parent_id=None,
                                                                    data=product_data)
            products_to_insert.extend(new_products_to_insert)

        products_serializer = ProductOrderSerializer(data=products_to_insert, many=True)

        if products_serializer.is_valid():
            return products_serializer.save()
        return None

    def select_all_products_datas(self, order, parent_id, data):
        id = data.get('id', None)
        sku = data.get('sku', None)
        products_list = []

        product_data_serializer = {'id': id,
                                   'sku': sku,
                                   'parent_item_id': parent_id if parent_id else id,
                                   'name': data.get('name', None),
                                   'quantity': data.get('quantity', None),
                                   'type': data.get('type', None),
                                   'comments': data.get('comments', None),
                                   'unit_price_with_discount': data.get('unit_price_with_discount', None),
                                   'unit_price_without_discount': data.get('unit_price_without_discount', None),
                                   'percentage_discount': data.get('percentage_discount', None),
                                   'order_id': order.id_pk}

        products_list.append(product_data_serializer)

        sub_products_data = data.get('subitems', [])
        for sub_product_data in sub_products_data:
            sub_product_inserted = self.select_all_products_datas(order=order, parent_id=id, data=sub_product_data)
            products_list.extend(sub_product_inserted)

        return products_list
