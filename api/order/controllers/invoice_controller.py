from ..serializers.invoice_serializer import InvoiceSerializer


class InvoiceController:

    def insert_invoice(self, data):
        invoice_data_serializer = {'company_name': data.get('company_name', None),
                                   'company_address': data.get('company_name', None),
                                   'tax_id': data.get('company_name', None)}
        invoice_serializer = InvoiceSerializer(data=invoice_data_serializer)
        if invoice_serializer.is_valid():
            return invoice_serializer.save()
        return None
