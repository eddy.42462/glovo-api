from ..serializers.billing_information_serializer import BillingInformationSerializer


class BillingInformationController:

    def insert_billing_info_rappi(self, data):
        # billing_info_data = {
        #     'billing_type': data.get('billing_type', None),
        #     'name': data.get('name', None),
        #     'address': data.get('address', None),
        #     'phone': data.get('phone', None),
        #     'email': data.get('email', None),
        #     'document_type': data.get('document_type', None),
        #     'document_number': data.get('document_number', None)
        # }
        billing_info_serializer = BillingInformationSerializer(data=data)
        if billing_info_serializer.is_valid():
            return billing_info_serializer.save()
        return None
