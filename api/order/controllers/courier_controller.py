from ..serializers.courier_serializer import CourierSerializer


class CourierController:

    def insert_courier(self, data):
        courier_data_serializer = {'name': data.get('name', None),
                                   'phone_number': data.get('phone_number', None)}
        courier_serializer = CourierSerializer(data=courier_data_serializer)
        if courier_serializer.is_valid():
            return courier_serializer.save()
        return None
