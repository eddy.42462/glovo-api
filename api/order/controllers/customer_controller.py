from ..serializers.customer_serializer import CustomerSerializer


class CustomerController:

    def insert_customer_rappi_order(self, data):
        # TODO definir el key del document number
        customer_data_serializer = {'phone_number': data.get('phone_number', None),
                                    'first_name': data.get('first_name', None),
                                    'user_type': data.get('user_type', None),
                                    'last_name': data.get('last_name', None),
                                    'email': data.get('email', None),
                                    'document_number': data.get('document_number', None)}
        customer_serializer = CustomerSerializer(data=customer_data_serializer)
        if customer_serializer.is_valid():
            return customer_serializer.save()
        return None
