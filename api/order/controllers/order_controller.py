from store.enum import ModeEnum, OrderStatusRappiEnum, OrderStatusPixelEnum, AggregatorEnum, TraceTypeEnum
from store.utils import insert_trace

from .billing_information_controller import BillingInformationController
from .customer_controller import CustomerController
from .delivery_discount_controller import DeliveryDiscountController
from .delivery_information_controller import DeliveryInformationController
from .product_order_controller import ProductOrderController
from .order_not_inserted_controller import OrderNotReceivedController
from ..serializers.order_serializer import OrderSerializer


class OrderController:
    billing_info_ctl = BillingInformationController()
    customer_ctl = CustomerController()
    delivery_discount_ctl = DeliveryDiscountController()
    delivery_info_ctl = DeliveryInformationController()
    product_order_ctl = ProductOrderController()
    order_not_inserted_ctl = OrderNotReceivedController()

    def insert_order_from_rappi(self, aggregator, store, order_data, mode):
        trace_type = TraceTypeEnum.ORDER_ADD
        order_json = order_data
        order_detail = order_data.get('order_detail', {})
        order_id = order_detail.get('order_id', None)

        customer_data = order_data.get('customer', {})
        products_data = order_detail.get('items', [])
        billing_information_data = order_detail.get('billing_information', {})
        totals_data = order_detail.get('totals', {})
        delivery_discount_data = order_detail.get('delivery_discount', {})
        delivery_info = order_detail.get('delivery_information', {})

        billing_info_inserted = None
        if billing_information_data:
            billing_info_inserted = self.billing_info_ctl.insert_billing_info_rappi(
                data=billing_information_data)

        customer_inserted = None
        if customer_data:
            customer_inserted = self.customer_ctl.insert_customer_rappi_order(
                data=customer_data)

        delivery_inserted = None
        if delivery_info:
            delivery_inserted = self.delivery_info_ctl.insert_delivery_info_rappi_order(
                data=delivery_info)

        delivery_discount_inserted = None
        if delivery_discount_data:
            delivery_discount_inserted = self.delivery_discount_ctl.insert_delivery_discount_rappi(
                data=delivery_discount_data)

        other_totals = totals_data.get('other_totals', {})
        charges = totals_data.get('charges', None)
        rappi_credits = None
        total_rappi_pay = None
        tip = None
        shipping = None
        service_fee = None

        if other_totals:
            rappi_credits = other_totals.get('total_rappi_credits', None)
            total_rappi_pay = other_totals.get('total_rappi_pay', None)
            tip = other_totals.get('tip', None)

        if charges:
            shipping = charges.get('shipping', None)
            service_fee = charges.get('service_fee', None)

        order_data_to_insert = {'order_id': order_detail.get('order_id', None),
                                'cooking_time': order_detail.get('cooking_time', None),
                                'min_cooking_time': order_detail.get('min_cooking_time', None),
                                'max_cooking_time': order_detail.get('max_cooking_time', None),
                                'created_at_rappi': order_detail.get('created_at', None),
                                'delivery_method': order_detail.get('delivery_method', None),
                                'payment_method': order_detail.get('payment_method', None),
                                'aggregator_id': aggregator.id_pk,
                                'customer_id': customer_inserted.id_pk if customer_inserted else None,
                                'delivery_information_id': delivery_inserted.id_pk if delivery_inserted else None,
                                'billing_information_id': billing_info_inserted.id_pk if billing_info_inserted else None,
                                'delivery_discount_id': delivery_discount_inserted.id_pk if delivery_discount_inserted else None,
                                'store_id': store.id_pk,
                                'status': OrderStatusRappiEnum.SENT.value,
                                'status_pixel': OrderStatusPixelEnum.CREATED.value,
                                'total_products_with_discount': totals_data.get(
                                    'total_products_with_discount', None),
                                'total_products_without_discount': totals_data.get(
                                    'total_products_without_discount', None),
                                'total_other_discounts': totals_data.get(
                                    'total_other_discounts', None),
                                'total_order': totals_data.get('total_order', None),
                                'total_to_pay': totals_data.get('total_to_pay', None),
                                'total_rappi_credits': rappi_credits,
                                'total_rappi_pay': total_rappi_pay,
                                'tip': tip,
                                'shipping': shipping,
                                'service_fee': service_fee,
                                'order_json': order_json,
                                'mode': mode.value}

        order_serializer = OrderSerializer(data=order_data_to_insert)

        if order_serializer.is_valid():
            try:
                order = order_serializer.save()
                if order:
                    if products_data and order:
                        self.product_order_ctl.insert_product_rappi(order=order,
                                                                    products_data=products_data)
                else:
                    comment = "Order with id {} not inserted with unknow cause".format(order_id)
                    self.order_not_inserted_ctl.insert_order_not_inserted(order_id=order_id,
                                                                          store_id=store.id_pk,
                                                                          aggregator_id=aggregator.id_pk,
                                                                          order_json=order_data,
                                                                          exception=comment,
                                                                          mode=mode.value)
                    insert_trace(trace_type=trace_type.value,
                                 exception=comment,
                                 instance_id=order_id,
                                 content=str(order_data),
                                 mode=mode.value)
            except Exception as e:
                self.order_not_inserted_ctl.insert_order_not_inserted(order_id=order_id,
                                                                      store_id=store.id_pk,
                                                                      aggregator_id=aggregator.id_pk,
                                                                      order_json=order_data,
                                                                      exception=str(e),
                                                                      mode=mode.value)
                insert_trace(trace_type=trace_type.value,
                             exception=str(e),
                             instance_id=order_id,
                             content=str(order_data),
                             mode=mode.value)

        else:
            exception = 'Error in the serializer of the Order with id {} errors details: {}'.format(order_id,
                                                                                                    str(
                                                                                                        order_serializer.errors))
            self.order_not_inserted_ctl.insert_order_not_inserted(order_id=order_id,
                                                                  store_id=store.id_pk,
                                                                  aggregator_id=aggregator.id_pk,
                                                                  order_json=order_data,
                                                                  exception=exception,
                                                                  mode=mode.value)
            insert_trace(trace_type=trace_type.value,
                         exception=exception,
                         instance_id=order_id,
                         content=str(order_data),
                         mode=mode.value)
            print("Errores del serializador de la orden con id {}:  errores: {}".format(order_id,
                                                                                        str(order_serializer.errors)))
