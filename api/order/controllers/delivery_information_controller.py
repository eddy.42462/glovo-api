from ..serializers.delivery_information_serializer import DeliveryInformationSerializer


class DeliveryInformationController:

    def insert_delivery_info_rappi_order(self, data):
        delivery_address_data_serializer = {'city': data.get('city', None),
                                            'complete_address': data.get('complete_address', None),
                                            'description': data.get('description', None),
                                            'neighborhood': data.get('neighborhood', None),
                                            'postal_code': data.get('postal_code', None),
                                            'complement': data.get('complement', None)}
        delivery_address_serializer = DeliveryInformationSerializer(data=delivery_address_data_serializer)
        if delivery_address_serializer.is_valid():
            return delivery_address_serializer.save()
        return None
