from store.enum import ModeEnum, TraceTypeEnum
from store.utils import insert_trace

from ..models import OrderBodyResponse


class OrderResponseBodyController:

    def insert_order_response_body_from_rappi(self, store, aggregator_inserted, body_response, mode):
        try:
            OrderBodyResponse.objects.create(**{'store': store,
                                                'aggregator': aggregator_inserted,
                                                'orders_json': body_response})
        except Exception as e:
            exception = "Error inserting a new OrderBodyResponse instance, details: {}".format(e)
            insert_trace(trace_type=TraceTypeEnum.ORDER_ADD_RESPONSE.value,
                         exception=exception,
                         content=str(body_response),
                         mode=mode.value)
