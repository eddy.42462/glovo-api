# Generated by Django 3.1 on 2021-06-25 00:03

from django.db import migrations
import django.db.models.manager


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0005_auto_20210604_0120'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='order',
            managers=[
                ('orders_success', django.db.models.manager.Manager()),
            ],
        ),
    ]
