from django.contrib import admin
from .models import *

admin.site.register(Customer)
admin.site.register(ProductOrder)
admin.site.register(Courier)
admin.site.register(DeliveryInformation)
admin.site.register(Invoice)
admin.site.register(Order)
admin.site.register(OrderCancelation)