from django import template

register = template.Library()


@register.filter(name='get_value')
def get_value(values_dict: dict, key: int):
    if not values_dict:
        return "Not found"
    return values_dict.get(key, "Not found")
