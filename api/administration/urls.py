from django.contrib.auth import views
from django.contrib.auth.decorators import login_required
from django.urls import path
from rest_framework import routers
from django.urls import include, path
from . import views as admin_views
from .services.order_view import OrderView
from .services.aggregator_view import AggregatorView
from .services.store_view import StoreView
from .services.menu_view import MenuView
from .forms import UserLoginForm

# router = routers.DefaultRouter()
# router.register(r'aggregator', AggregatorView, basename='aggregator')
# router.register(r'store', StoreView, basename='store')
# router.register(r'menu', MenuView, basename='menu')
# router.register(r'order', OrderView, basename='order')

urlpatterns = [
    path('index', login_required(admin_views.index_view), name="index"),
    path('login', views.LoginView.as_view(template_name="app/login.html", authentication_form=UserLoginForm),
         name='login'),
    path('logout', login_required(admin_views.logout_view), name='logout'),
    # path('', include(router.urls)),
    path('user', login_required(admin_views.user_list), name="user_list"),
    path('user/add', login_required(admin_views.user_add), name="user_add"),
    path('user/edit/<str:id>', login_required(admin_views.user_edit), name="user_edit"),
    path('store/add', login_required(admin_views.store_add), name="store_add"),
    path('store/edit/<str:id>', login_required(admin_views.store_edit), name="store_edit"),
    path('store/detail/<str:id>', login_required(admin_views.store_detail), name="store_detail"),
    path('aggregator/add', login_required(admin_views.aggregator_add), name="aggregator_add"),
    path('aggregator/edit/<str:id_pk>', login_required(admin_views.aggregator_edit), name="aggregator_edit"),
    path('aggregator', login_required(admin_views.aggregators_list), name="aggregators_list"),
    path('menu', login_required(admin_views.menus_list), name="menus_list"),
    path('menu/detail/<str:id>', login_required(admin_views.menu_detail), name="menu_detail"),
    path('order', login_required(admin_views.orders_list), name="orders_list"),
    path('order/canceled', login_required(admin_views.orders_canceled_list), name="orders_canceled_list"),
    path('order/not_received', login_required(admin_views.orders_not_received_list), name="orders_not_received_list"),
]
