from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import get_user_model


class UserLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)

    username = forms.CharField(widget=forms.TextInput(
        attrs={'id': 'username_field',
               'class': 'form-control',
               'placeholder': 'Username'
               }))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'id': 'passrowd_field',
            'class': 'form-control',
            'placeholder': 'Password'
        }
    ))


class CustomUserCreationForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)

    username = forms.CharField(widget=forms.TextInput(
        attrs={'id': 'username',
               'name': 'username',
               'class': 'form-control col-md-7 col-xs-12',
               'required': 'required'
               }))

    email = forms.EmailField(widget=forms.EmailInput(
        attrs={
            'id': 'input-email',
            'name': 'email',
            'class': 'form-control col-md-7 col-xs-12',
        }), required=False)

    first_name = forms.CharField(widget=forms.TextInput(
        attrs={'id': 'input-first-name',
               'name': 'first_name',
               'class': 'form-control col-md-7 col-xs-12',
               }), required=False)

    last_name = forms.CharField(widget=forms.TextInput(
        attrs={
            'id': 'input-last-name',
            'name': 'last_name',
            'class': 'form-control col-md-7 col-xs-12',
        }), required=False)

    is_active = forms.BooleanField(widget=forms.CheckboxInput(
        attrs={
            'id': 'is-active-field',
            'name': 'is_active',
        }), required=False)

    is_staff = forms.BooleanField(widget=forms.CheckboxInput(
        attrs={
            'id': 'is-staff-field',
            'name': 'is_staff',
        }), required=False)

    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'id': 'input-password1',
            'name': 'password1',
            'class': 'form-control col-md-7 col-xs-12',
            'required': 'required'
        }))

    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={
            'id': 'input-password2',
            'name': 'password2',
            'class': 'form-control col-md-7 col-xs-12',
            'required': 'required'
        }))


class CustomUserEditionForm(UserChangeForm):
    class Meta:
        model = get_user_model()
        fields = ('username', 'email', 'first_name', 'last_name', 'is_active', 'is_staff')

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)

    username = forms.CharField(widget=forms.TextInput(
        attrs={
            'id': 'username',
            'name': 'username',
            'class': 'form-control col-md-7 col-xs-12',
            'required': 'required'
        }))

    email = forms.EmailField(widget=forms.EmailInput(
        attrs={
            'id': 'input-email',
            'name': 'email',
            'class': 'form-control col-md-7 col-xs-12',
        }), required=False)

    first_name = forms.CharField(widget=forms.TextInput(
        attrs={
            'id': 'input-first-name',
            'name': 'first_name',
            'class': 'form-control col-md-7 col-xs-12',
        }), required=False)

    last_name = forms.CharField(widget=forms.TextInput(
        attrs={
            'id': 'input-last-name',
            'name': 'last_name',
            'class': 'form-control col-md-7 col-xs-12',
        }), required=False)

    is_active = forms.BooleanField(widget=forms.CheckboxInput(
        attrs={
            'id': 'is-active-field',
            'name': 'is_active',
        }), required=False)

    is_staff = forms.BooleanField(widget=forms.CheckboxInput(
        attrs={
            'id': 'is-staff-field',
            'name': 'is_staff',
        }), required=False)
