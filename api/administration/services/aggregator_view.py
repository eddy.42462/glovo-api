from django.shortcuts import render
from order.notifications_templates import ALREADY_EXIST_AGGREGATOR_CODE, ALREADY_EXIST_AGGREGATOR_NAME, \
    NOT_EXIST_AGGREGATOR_ID, INSERT_AGGREGATOR_WITHOUT_NAME, INSERT_AGGREGATOR_WITHOUT_CODE
from rest_framework import status
from rest_framework import viewsets
from store.models import Aggregator

from ..bad_request import BadRequest
from ..error_type import ErrorType
from ..request_error import RequestError
from ..serializers import AggregatorSerializer


class AggregatorView(viewsets.ReadOnlyModelViewSet):
    serializer_class = AggregatorSerializer

    def aggregators_list(self, request):
        """
        Render to list of aggregators
        """
        aggregators = Aggregator.objects.all().order_by('name')
        context = {'aggregators': aggregators}
        return render(request, "aggregator/aggregator_list.html", context)

    def aggregator_add(self, request):
        """
        Allow add an Aggregator
        """
        if request.method == 'POST':
            aggregator_data = {'code': request.POST.get('code'),
                               'name': request.POST.get('name'),
                               'production_url': request.POST.get('input-production-url', None),
                               'develop_url': request.POST.get('input-develop-url', None)}

            if Aggregator.objects.filter(name=request.POST.get('name')):
                error_mssg = ALREADY_EXIST_AGGREGATOR_NAME.format(request.POST.get('name'))
                return BadRequest(http_status_code=status.HTTP_409_CONFLICT,
                                  exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

            if Aggregator.objects.filter(code=request.POST.get('code')):
                error_mssg = ALREADY_EXIST_AGGREGATOR_CODE.format(request.POST.get('code'))
                return BadRequest(http_status_code=status.HTTP_409_CONFLICT,
                                  exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

            aggregator_serializer = AggregatorSerializer(data=aggregator_data)

            if aggregator_serializer.is_valid():
                aggregator_serializer.save()

            aggregators = Aggregator.objects.all().order_by('name')
            context = {'aggregators': aggregators}
            return render(request, 'aggregator/aggregator_list.html', context)

        elif request.method == 'GET':
            context = {'create': True}
            return render(request, 'aggregator/aggregator_form.html', context)

    def aggregator_edit(self, request, id_pk):
        """
        Allow edit an Aggregator
        """
        context = {}
        try:
            aggregator = Aggregator.objects.get(id_pk=id_pk)
        except:
            error_mssg = NOT_EXIST_AGGREGATOR_ID.format(id_pk)
            return BadRequest(http_status_code=status.HTTP_404_NOT_FOUND,
                              exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

        if request.method == 'POST':
            name = request.POST.get('name', None)
            code = request.POST.get('code', None)

            if not name:
                return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                  exception=RequestError(error_type=ErrorType.ERROR,
                                                         message=INSERT_AGGREGATOR_WITHOUT_NAME))

            if not code:
                return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                  exception=RequestError(error_type=ErrorType.ERROR,
                                                         message=INSERT_AGGREGATOR_WITHOUT_CODE))

            if aggregator.name != name and Aggregator.objects.filter(name=name):
                error_mssg = ALREADY_EXIST_AGGREGATOR_NAME.format(name)
                return BadRequest(http_status_code=status.HTTP_409_CONFLICT,
                                  exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

            if aggregator.code != code and Aggregator.objects.filter(code=code):
                error_mssg = ALREADY_EXIST_AGGREGATOR_CODE.format(code)
                return BadRequest(http_status_code=status.HTTP_409_CONFLICT,
                                  exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

            aggregator_data = {'code': code,
                               'name': name,
                               'production_url': request.POST.get('input-production-url', None),
                               'develop_url': request.POST.get('input-develop-url', None)}

            aggregator, _ = Aggregator.objects.update_or_create(id_pk=id_pk,
                                                                defaults=aggregator_data)

            aggregators = Aggregator.objects.all().order_by('name')
            context = {'aggregators': aggregators}
            return render(request, 'aggregator/aggregator_list.html', context)

        elif request.method == 'GET':
            context = {'aggregator': aggregator}

        return render(request, 'aggregator/aggregator_form.html', context)
