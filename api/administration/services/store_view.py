import json

from django.db.models import Q
from django.shortcuts import render
from order.notifications_templates import INSERT_STORE_WITHOUT_RUC, INSERT_STORE_WITHOUT_STABLISHMENT_NUMBER, \
    ALREADY_EXIST_STORE_ID, ERROR_CREDENTIALS, NOT_EXIST_STORE_ID, EDIT_STORE_WITHOUT_RUC, NOT_EXIST_AGGREGATOR_ID, \
    CANT_INSERT_STORE, EDIT_STORE_WITHOUT_STABLISHMENT_NUMBER
from rest_framework import status
from rest_framework import viewsets
from store.models import Store, Aggregator, StoreAggregator, Token

from ..bad_request import BadRequest
from ..error_type import ErrorType
from ..request_error import RequestError
from ..serializers import StoreSerializer


class StoreView(viewsets.ReadOnlyModelViewSet):
    serializer_class = StoreSerializer

    def store_add(self, request):
        """
        Allows add or edit a Store
        """
        if request.method == 'POST':
            if not request.POST.get('ruc'):
                return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                  exception=RequestError(error_type=ErrorType.ERROR, message=INSERT_STORE_WITHOUT_RUC))

            if not request.POST.get('stablishment_number'):
                return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                  exception=RequestError(error_type=ErrorType.ERROR,
                                                         message=INSERT_STORE_WITHOUT_STABLISHMENT_NUMBER))

            integrationId = request.POST.get('ruc') + '-' + request.POST.get('stablishment_number')
            if integrationId and Store.objects.filter(integrationId=integrationId):
                return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                  exception=RequestError(error_type=ErrorType.ERROR,
                                                         message=ALREADY_EXIST_STORE_ID.format(integrationId)))

            store_data = {'integrationId': integrationId,
                          'name': request.POST.get('name'),
                          'active': True if request.POST.get('active') else False,
                          'production_mode': True if request.POST.get('active-production-mode') else False,
                          'develop_mode': True if request.POST.get('active-develop-mode') else False}

            store_serializer = StoreSerializer(data=store_data)

            if store_serializer.is_valid():
                store = store_serializer.save()

                aggregators_selected = request.POST.getlist('aggregators', [])
                store_aggregator_list = []
                for aggregator_id in aggregators_selected:
                    aggregator_inserted = Aggregator.objects.get(id_pk=aggregator_id)

                    relation_id = request.POST.get(aggregator_inserted.name) if request.POST.get(
                        aggregator_inserted.name) else None
                    production_credentials = request.POST.get('production-credentials-' + aggregator_inserted.name)
                    develop_credentials = request.POST.get('develop-credentials-' + aggregator_inserted.name)

                    # prod_cred_dict = {}
                    # dev_cred_dict = {}
                    try:
                        prod_cred_dict = json.loads(production_credentials) if production_credentials else None
                    except:
                        return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                          exception=RequestError(error_type=ErrorType.ERROR,
                                                                 message=ERROR_CREDENTIALS.format('producción')))

                    try:
                        dev_cred_dict = json.loads(develop_credentials) if develop_credentials else None
                    except:
                        return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                          exception=RequestError(error_type=ErrorType.ERROR,
                                                                 message=ERROR_CREDENTIALS.format('desarrollo')))

                    store_aggregator = StoreAggregator(store=store, aggregator=aggregator_inserted,
                                                       relation_id=relation_id,
                                                       production_credentials=prod_cred_dict,
                                                       develop_credentials=dev_cred_dict)

                    store_aggregator_list.append(store_aggregator)

                StoreAggregator.objects.bulk_create(store_aggregator_list)

            else:
                return BadRequest(http_status_code=status.HTTP_409_CONFLICT,
                                  exception=RequestError(error_type=ErrorType.ERROR, message=CANT_INSERT_STORE))

            context = {'stores': self.get_stores_data()}
            return render(request, 'store/store_list.html', context)

        elif request.method == 'GET':
            context = {'create': True,
                       'aggregators': Aggregator.objects.all().order_by('name')}

            return render(request, 'store/store_form.html', context)

    def store_edit(self, request, id):
        context = {}
        try:
            store = Store.objects.get(id_pk=id)
        except:
            error_mssg = NOT_EXIST_STORE_ID.format(id)
            return BadRequest(http_status_code=status.HTTP_404_NOT_FOUND,
                              exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

        if request.method == 'POST':
            if not request.POST.get('ruc'):
                return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                  exception=RequestError(error_type=ErrorType.ERROR, message=EDIT_STORE_WITHOUT_RUC))

            if not request.POST.get('stablishment_number'):
                return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                  exception=RequestError(error_type=ErrorType.ERROR,
                                                         message=EDIT_STORE_WITHOUT_STABLISHMENT_NUMBER))

            new_integration_id = request.POST.get('ruc') + '-' + request.POST.get('stablishment_number')
            if new_integration_id and Store.objects.filter(~Q(id_pk=id), integrationId=new_integration_id):
                error_mssg = ALREADY_EXIST_STORE_ID.format(new_integration_id)
                return BadRequest(http_status_code=status.HTTP_409_CONFLICT,
                                  exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

            store_data = {'integrationId': new_integration_id,
                          'name': request.POST.get('name'),
                          'active': True if request.POST.get('active') else False,
                          'production_mode': True if request.POST.get('active-production-mode') else False,
                          'develop_mode': True if request.POST.get('active-develop-mode') else False}

            if new_integration_id:
                store, _ = Store.objects.update_or_create(id_pk=id, defaults=store_data)

            if store:
                Token.objects.filter(id=id).delete()

                aggregators_selected = request.POST.getlist('aggregators', [])

                store_aggregator_list = []
                for aggregator_id in aggregators_selected:
                    try:
                        aggregator_inserted = Aggregator.objects.get(id_pk=aggregator_id)

                        relation_id = request.POST.get(aggregator_inserted.name) if request.POST.get(
                            aggregator_inserted.name) else None
                        production_credentials = request.POST.get('production-credentials-' + aggregator_inserted.name)
                        develop_credentials = request.POST.get('develop-credentials-' + aggregator_inserted.name)

                        try:
                            prod_cred_dict = json.loads(production_credentials) if production_credentials else None
                        except:
                            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                              exception=RequestError(error_type=ErrorType.ERROR,
                                                                     message=ERROR_CREDENTIALS.format('producción')))

                        try:
                            dev_cred_dict = json.loads(develop_credentials) if develop_credentials else None
                        except:
                            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                              exception=RequestError(error_type=ErrorType.ERROR,
                                                                     message=ERROR_CREDENTIALS.format('desarrollo')))

                        store_aggregator = StoreAggregator(store=store, aggregator=aggregator_inserted,
                                                           relation_id=relation_id,
                                                           production_credentials=prod_cred_dict,
                                                           develop_credentials=dev_cred_dict)
                        store_aggregator_list.append(store_aggregator)

                    except:
                        error_mssg = NOT_EXIST_AGGREGATOR_ID.format(aggregator_id)
                        return BadRequest(http_status_code=status.HTTP_404_NOT_FOUND,
                                          exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

                store.aggregator.clear()
                StoreAggregator.objects.bulk_create(store_aggregator_list)

            context = {'stores': self.get_stores_data()}
            return render(request, 'store/store_list.html', context)

        elif request.method == 'GET':
            ids = store.integrationId.split('-')

            values = []
            aggregators = Aggregator.objects.values('id_pk', 'name', 'code').order_by('name')
            for aggregator in aggregators:
                store_aggregator = StoreAggregator.objects.filter(store__id_pk=id,
                                                                  aggregator__id_pk=aggregator['id_pk'])
                values.append({'id_pk': aggregator['id_pk'],
                               'name': aggregator['name'],
                               'code': aggregator['code'],
                               'store_relation': store_aggregator[0].store.integrationId if store_aggregator else None,
                               'relation_id': store_aggregator[0].relation_id if store_aggregator else None,
                               'production_credentials': json.dumps(store_aggregator[
                                                                        0].production_credentials) if store_aggregator and
                                                                                                      store_aggregator[
                                                                                                          0].production_credentials else None,
                               'develop_credentials': json.dumps(store_aggregator[
                                                                     0].develop_credentials) if store_aggregator and
                                                                                                store_aggregator[
                                                                                                    0].develop_credentials else None})

            context = {'create': False,
                       'store': store,
                       'ruc': ids[0],
                       'stablishment_number': ids[1] if len(ids) > 1 else '',
                       'aggregators': values}

        return render(request, 'store/store_form.html', context)

    def store_detail(sefl, request, id):
        """

        """
        if request.method == 'GET':
            try:
                store = Store.objects.get(id_pk=id)
            except:
                error_mssg = NOT_EXIST_STORE_ID.format(id)
                return BadRequest(http_status_code=status.HTTP_404_NOT_FOUND,
                                  exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

            ids = store.integrationId.split('-')
            context = {'store': store,
                       'ruc': ids[0],
                       'stablishment_number': ids[1] if len(ids) > 1 else '',
                       'store_aggregators': store.storeaggregator_set.all()}
            return render(request, 'store/store_detail.html', context)

    def get_stores_data(self):
        stores_data_list = []
        stores = Store.objects.all().order_by('name')
        for store in stores:
            aggregators_names = self.get_aggregators_string(store.aggregator.values_list('name', flat=True))
            stores_data_list.append({'id': store.id_pk,
                                     'integrationId': store.integrationId,
                                     'name': store.name,
                                     'active': store.active,
                                     'production_mode': store.production_mode,
                                     'develop_mode': store.develop_mode,
                                     'aggregators': aggregators_names})
        return stores_data_list

    def get_aggregators_string(self, aggregators: []):
        aggregators_concat = ''
        for aggregator in aggregators:
            aggregators_concat = aggregators_concat + ', ' + aggregator
        return aggregators_concat[1:]
