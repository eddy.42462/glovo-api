from datetime import datetime, timedelta

from django.shortcuts import render
from order.models import Order, OrderNotReceived
from order.serializers.order_serializer import OrderSerializer
from rest_framework import status
from rest_framework import viewsets
from store.dicts import dict_status_rappi, dict_status_pixel, dict_status_final, dict_mode
from store.enum import OrderStatusFinalEnum
from store.models import Store, Aggregator

from ..bad_request import BadRequest
from ..error_type import ErrorType
from ..request_error import RequestError


class OrderView(viewsets.ReadOnlyModelViewSet):
    serializer_class = OrderSerializer

    def show_list(self, request):
        context = {'status_canceled': OrderStatusFinalEnum.CANCELED.value,
                   'dict_status_rappi': dict_status_rappi,
                   'dict_status_pixel': dict_status_pixel,
                   'dict_status_final': dict_status_final,
                   'dict_mode': dict_mode}
        return render(request, "order/order_list.html", context)

    def orders_list(self, request):
        """
        Render to list of orders
        """
        start = request.GET.get('start', None)
        end = request.GET.get('end', None)

        try:
            datetime_start = datetime.strptime(start, '%d-%m-%Y') if start else None
            datetime_end = datetime.strptime(end, '%d-%m-%Y') if end else None
        except Exception as e:
            error_mssg = "Error en los formatos de las fechas"
            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                              exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

        if datetime_start and datetime_end:
            orders = Order.objects.filter(created_at__range=[datetime_start, datetime_end]).values('order_id', 'status',
                                                                                                   'transaction_number',
                                                                                                   'status_pixel',
                                                                                                   'status_final',
                                                                                                   'store__name',
                                                                                                   'store__integrationId',
                                                                                                   'aggregator',
                                                                                                   'created_at',
                                                                                                   'processed_at_pixel',
                                                                                                   'aggregator__name',
                                                                                                   'mode').order_by(
                '-created_at')
        elif datetime_start:
            orders = Order.objects.filter(created_at__gte=datetime_start).values('order_id', 'status',
                                                                                 'transaction_number',
                                                                                 'status_pixel', 'status_final',
                                                                                 'store__name',
                                                                                 'store__integrationId', 'aggregator',
                                                                                 'created_at',
                                                                                 'processed_at_pixel',
                                                                                 'aggregator__name',
                                                                                 'mode').order_by(
                '-created_at')
        else:
            last_day = datetime.now() - timedelta(days=1)
            orders = Order.objects.filter(created_at__gte=last_day).values('order_id', 'status', 'transaction_number',
                                                                           'status_pixel', 'status_final',
                                                                           'store__name',
                                                                           'store__integrationId', 'aggregator',
                                                                           'created_at',
                                                                           'processed_at_pixel', 'aggregator__name',
                                                                           'mode').order_by(
                '-created_at')
        context = {'orders': orders,
                   'status_canceled': OrderStatusFinalEnum.CANCELED.value,
                   'dict_status_rappi': dict_status_rappi,
                   'dict_status_pixel': dict_status_pixel,
                   'dict_status_final': dict_status_final,
                   'dict_mode': dict_mode}
        return render(request, "order/order_list.html", context)

    def orders_not_received_list(self, request):
        """
        Render to list of orders
        """
        dict_stores = {}
        stores = Store.objects.all()
        for store in stores:
            dict_stores[store.id_pk] = store.integrationId

        dict_aggregators = {}
        aggregators = Aggregator.objects.all()
        for aggregator in aggregators:
            dict_aggregators[aggregator.id_pk] = aggregator.name
        orders = OrderNotReceived.objects.all().values('order_id', 'store_id', 'aggregator_id',
                                                       'created_at', 'exception', 'mode').order_by('-created_at')
        context = {'orders': orders,
                   'dict_status_rappi': dict_status_rappi,
                   'dict_status_pixel': dict_status_pixel,
                   'dict_status_final': dict_status_final,
                   'dict_mode': dict_mode,
                   'dict_aggregators': dict_aggregators,
                   'dict_stores': dict_stores}
        return render(request, "order_not_received/order_not_received_list.html", context)

    def orders_canceled_list(self, request):
        """
        Render to list of orders canceled
        """
        orders = Order.objects.filter(status_final=OrderStatusFinalEnum.CANCELED.value)
        context = {'orders': orders,
                   'dict_status_rappi': dict_status_rappi,
                   'dict_status_pixel': dict_status_pixel,
                   'dict_status_final': dict_status_final,
                   'dict_mode': dict_mode}
        return render(request, "order_canceled/order_canceled_list.html", context)
