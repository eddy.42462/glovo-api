from django.shortcuts import render
from menu.models import Menu
from rest_framework import viewsets
from store.dicts import dict_mode

from ..serializers import AggregatorSerializer


class MenuView(viewsets.ReadOnlyModelViewSet):
    serializer_class = AggregatorSerializer

    def menus_list(self, request):
        """
        Render to list of menus
        """
        menus = Menu.objects.all().order_by('-created_at')
        context = {'menus': menus,
                   'dict_mode': dict_mode}
        return render(request, "menu/menu_list.html", context)
