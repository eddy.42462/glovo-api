import json
from datetime import datetime, timedelta

from django.contrib.auth import get_user_model
from django.contrib.auth import logout
from django.db.models import Q
from django.shortcuts import redirect
from django.shortcuts import render
from menu.models import Menu
from order.models import Order, OrderNotReceived
from order.notifications_templates import *
from rest_framework import status
from store.dicts import dict_status_rappi, dict_status_pixel, dict_status_final, dict_mode
from store.enum import OrderStatusFinalEnum
from store.models import Store, Aggregator, StoreAggregator, Token

from .bad_request import BadRequest
from .error_type import ErrorType
from .forms import CustomUserCreationForm, CustomUserEditionForm
from .request_error import RequestError
from .serializers import StoreSerializer, AggregatorSerializer


def index_view(request):
    """
    Render to list of Stores
    """
    context = {'stores': get_stores_data()}
    return render(request, "store/store_list.html", context)


def get_aggregators_string(aggregators: []):
    aggregators_concat = ''
    for aggregator in aggregators:
        aggregators_concat = aggregators_concat + ', ' + aggregator
    return aggregators_concat[1:]


def get_stores_data():
    stores_data_list = []
    stores = Store.objects.all().order_by('name')
    for store in stores:
        aggregators_names = get_aggregators_string(store.aggregator.values_list('name', flat=True))
        stores_data_list.append({'id': store.id_pk,
                                 'integrationId': store.integrationId,
                                 'name': store.name,
                                 'active': store.active,
                                 'production_mode': store.production_mode,
                                 'develop_mode': store.develop_mode,
                                 'aggregators': aggregators_names})
    return stores_data_list


def get_aggregators_data():
    aggregators = Aggregator.objects.all().order_by('name')
    context = {'aggregators': aggregators}
    return context


def store_add(request):
    """
    Allows add or edit a Store
    """
    if request.method == 'POST':
        if not request.POST.get('ruc'):
            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                              exception=RequestError(error_type=ErrorType.ERROR, message=INSERT_STORE_WITHOUT_RUC))

        if not request.POST.get('stablishment_number'):
            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                              exception=RequestError(error_type=ErrorType.ERROR,
                                                     message=INSERT_STORE_WITHOUT_STABLISHMENT_NUMBER))

        integrationId = request.POST.get('ruc') + '-' + request.POST.get('stablishment_number')
        if integrationId and Store.objects.filter(integrationId=integrationId):
            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                              exception=RequestError(error_type=ErrorType.ERROR,
                                                     message=ALREADY_EXIST_STORE_ID.format(integrationId)))

        store_data = {'integrationId': integrationId,
                      'name': request.POST.get('name'),
                      'active': True if request.POST.get('active') else False,
                      'production_mode': True if request.POST.get('active-production-mode') else False,
                      'develop_mode': True if request.POST.get('active-develop-mode') else False}

        store_serializer = StoreSerializer(data=store_data)

        if store_serializer.is_valid():
            store = store_serializer.save()

            aggregators_selected = request.POST.getlist('aggregators', [])
            store_aggregator_list = []
            for aggregator_id in aggregators_selected:
                aggregator_inserted = Aggregator.objects.get(id_pk=aggregator_id)

                relation_id = request.POST.get(aggregator_inserted.name) if request.POST.get(
                    aggregator_inserted.name) else None
                production_credentials = request.POST.get('production-credentials-' + aggregator_inserted.name)
                develop_credentials = request.POST.get('develop-credentials-' + aggregator_inserted.name)

                # prod_cred_dict = {}
                # dev_cred_dict = {}
                try:
                    prod_cred_dict = json.loads(production_credentials) if production_credentials else None
                except:
                    return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                      exception=RequestError(error_type=ErrorType.ERROR,
                                                             message=ERROR_CREDENTIALS.format('producción')))

                try:
                    dev_cred_dict = json.loads(develop_credentials) if develop_credentials else None
                except:
                    return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                      exception=RequestError(error_type=ErrorType.ERROR,
                                                             message=ERROR_CREDENTIALS.format('desarrollo')))

                store_aggregator = StoreAggregator(store=store, aggregator=aggregator_inserted,
                                                   relation_id=relation_id,
                                                   production_credentials=prod_cred_dict,
                                                   develop_credentials=dev_cred_dict)

                store_aggregator_list.append(store_aggregator)

            StoreAggregator.objects.bulk_create(store_aggregator_list)

        else:
            return BadRequest(http_status_code=status.HTTP_409_CONFLICT,
                              exception=RequestError(error_type=ErrorType.ERROR, message=CANT_INSERT_STORE))

        context = {'stores': get_stores_data()}
        return render(request, 'store/store_list.html', context)

    elif request.method == 'GET':
        context = {'create': True,
                   'aggregators': Aggregator.objects.all().order_by('name')}

        return render(request, 'store/store_form.html', context)


def store_edit(request, id):
    context = {}
    try:
        store = Store.objects.get(id_pk=id)
    except:
        error_mssg = NOT_EXIST_STORE_ID.format(id)
        return BadRequest(http_status_code=status.HTTP_404_NOT_FOUND,
                          exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

    if request.method == 'POST':
        if not request.POST.get('ruc'):
            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                              exception=RequestError(error_type=ErrorType.ERROR, message=EDIT_STORE_WITHOUT_RUC))

        if not request.POST.get('stablishment_number'):
            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                              exception=RequestError(error_type=ErrorType.ERROR,
                                                     message=EDIT_STORE_WITHOUT_STABLISHMENT_NUMBER))

        new_integration_id = request.POST.get('ruc') + '-' + request.POST.get('stablishment_number')
        if new_integration_id and Store.objects.filter(~Q(id_pk=id), integrationId=new_integration_id):
            error_mssg = ALREADY_EXIST_STORE_ID.format(new_integration_id)
            return BadRequest(http_status_code=status.HTTP_409_CONFLICT,
                              exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

        store_data = {'integrationId': new_integration_id,
                      'name': request.POST.get('name'),
                      'active': True if request.POST.get('active') else False,
                      'production_mode': True if request.POST.get('active-production-mode') else False,
                      'develop_mode': True if request.POST.get('active-develop-mode') else False}

        if new_integration_id:
            store, _ = Store.objects.update_or_create(id_pk=id, defaults=store_data)

        if store:
            Token.objects.filter(id=id).delete()

            aggregators_selected = request.POST.getlist('aggregators', [])

            store_aggregator_list = []
            for aggregator_id in aggregators_selected:
                try:
                    aggregator_inserted = Aggregator.objects.get(id_pk=aggregator_id)

                    relation_id = request.POST.get(aggregator_inserted.name) if request.POST.get(
                        aggregator_inserted.name) else None
                    production_credentials = request.POST.get('production-credentials-' + aggregator_inserted.name)
                    develop_credentials = request.POST.get('develop-credentials-' + aggregator_inserted.name)

                    try:
                        prod_cred_dict = json.loads(production_credentials) if production_credentials else None
                    except:
                        return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                          exception=RequestError(error_type=ErrorType.ERROR,
                                                                 message=ERROR_CREDENTIALS.format('producción')))

                    try:
                        dev_cred_dict = json.loads(develop_credentials) if develop_credentials else None
                    except:
                        return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                                          exception=RequestError(error_type=ErrorType.ERROR,
                                                                 message=ERROR_CREDENTIALS.format('desarrollo')))

                    store_aggregator = StoreAggregator(store=store, aggregator=aggregator_inserted,
                                                       relation_id=relation_id,
                                                       production_credentials=prod_cred_dict,
                                                       develop_credentials=dev_cred_dict)
                    store_aggregator_list.append(store_aggregator)

                except:
                    error_mssg = NOT_EXIST_AGGREGATOR_ID.format(aggregator_id)
                    return BadRequest(http_status_code=status.HTTP_404_NOT_FOUND,
                                      exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

            store.aggregator.clear()
            StoreAggregator.objects.bulk_create(store_aggregator_list)

        context = {'stores': get_stores_data()}
        return render(request, 'store/store_list.html', context)

    elif request.method == 'GET':
        ids = store.integrationId.split('-')

        values = []
        aggregators = Aggregator.objects.values('id_pk', 'name', 'code').order_by('name')
        for aggregator in aggregators:
            store_aggregator = StoreAggregator.objects.filter(store__id_pk=id,
                                                              aggregator__id_pk=aggregator['id_pk'])
            values.append({'id_pk': aggregator['id_pk'],
                           'name': aggregator['name'],
                           'code': aggregator['code'],
                           'store_relation': store_aggregator[0].store.integrationId if store_aggregator else None,
                           'relation_id': store_aggregator[0].relation_id if store_aggregator else None,
                           'production_credentials': json.dumps(store_aggregator[
                                                                    0].production_credentials) if store_aggregator and
                                                                                                  store_aggregator[
                                                                                                      0].production_credentials else None,
                           'develop_credentials': json.dumps(store_aggregator[
                                                                 0].develop_credentials) if store_aggregator and
                                                                                            store_aggregator[
                                                                                                0].develop_credentials else None})

        context = {'create': False,
                   'store': store,
                   'ruc': ids[0],
                   'stablishment_number': ids[1] if len(ids) > 1 else '',
                   'aggregators': values}

    return render(request, 'store/store_form.html', context)


def store_detail(request, id):
    """

    """
    if request.method == 'GET':
        try:
            store = Store.objects.get(id_pk=id)
        except:
            error_mssg = NOT_EXIST_STORE_ID.format(id)
            return BadRequest(http_status_code=status.HTTP_404_NOT_FOUND,
                              exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

        ids = store.integrationId.split('-')
        context = {'store': store,
                   'ruc': ids[0],
                   'stablishment_number': ids[1] if len(ids) > 1 else '',
                   'store_aggregators': store.storeaggregator_set.all()}
        return render(request, 'store/store_detail.html', context)


def aggregator_add(request):
    """
    Allow add an Aggregator
    """
    if request.method == 'POST':
        aggregator_data = {'code': request.POST.get('code'),
                           'name': request.POST.get('name'),
                           'production_url': request.POST.get('input-production-url', None),
                           'develop_url': request.POST.get('input-develop-url', None)}

        if Aggregator.objects.filter(name=request.POST.get('name')):
            error_mssg = ALREADY_EXIST_AGGREGATOR_NAME.format(request.POST.get('name'))
            return BadRequest(http_status_code=status.HTTP_409_CONFLICT,
                              exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

        if Aggregator.objects.filter(code=request.POST.get('code')):
            error_mssg = ALREADY_EXIST_AGGREGATOR_CODE.format(request.POST.get('code'))
            return BadRequest(http_status_code=status.HTTP_409_CONFLICT,
                              exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

        aggregator_serializer = AggregatorSerializer(data=aggregator_data)

        if aggregator_serializer.is_valid():
            aggregator_serializer.save()

        context = get_aggregators_data()
        return render(request, 'aggregator/aggregator_list.html', context)

    elif request.method == 'GET':
        context = {'create': True}
        return render(request, 'aggregator/aggregator_form.html', context)


def aggregator_edit(request, id_pk):
    """
    Allow edit an Aggregator
    """
    context = {}
    try:
        aggregator = Aggregator.objects.get(id_pk=id_pk)
    except:
        error_mssg = NOT_EXIST_AGGREGATOR_ID.format(id_pk)
        return BadRequest(http_status_code=status.HTTP_404_NOT_FOUND,
                          exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

    if request.method == 'POST':
        name = request.POST.get('name', None)
        code = request.POST.get('code', None)

        if not name:
            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                              exception=RequestError(error_type=ErrorType.ERROR,
                                                     message=INSERT_AGGREGATOR_WITHOUT_NAME))

        if not code:
            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                              exception=RequestError(error_type=ErrorType.ERROR,
                                                     message=INSERT_AGGREGATOR_WITHOUT_CODE))

        if aggregator.name != name and Aggregator.objects.filter(name=name):
            error_mssg = ALREADY_EXIST_AGGREGATOR_NAME.format(name)
            return BadRequest(http_status_code=status.HTTP_409_CONFLICT,
                              exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

        if aggregator.code != code and Aggregator.objects.filter(code=code):
            error_mssg = ALREADY_EXIST_AGGREGATOR_CODE.format(code)
            return BadRequest(http_status_code=status.HTTP_409_CONFLICT,
                              exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

        aggregator_data = {'code': code,
                           'name': name,
                           'production_url': request.POST.get('input-production-url', None),
                           'develop_url': request.POST.get('input-develop-url', None)}

        aggregator, _ = Aggregator.objects.update_or_create(id_pk=id_pk,
                                                            defaults=aggregator_data)

        context = get_aggregators_data()
        return render(request, 'aggregator/aggregator_list.html', context)

    elif request.method == 'GET':
        context = {'aggregator': aggregator}

    return render(request, 'aggregator/aggregator_form.html', context)


def aggregators_list(request):
    """
    Render to list of aggregators
    """
    context = get_aggregators_data()
    return render(request, "aggregator/aggregator_list.html", context)


def menus_list(request):
    """
    Render to list of menus
    """
    menus = Menu.objects.all().order_by('-created_at')
    context = {'menus': menus,
               'dict_mode': dict_mode}
    return render(request, "menu/menu_list.html", context)


def menu_detail(request, id):
    """

    """
    try:
        menu = Menu.objects.get(menu_id=id)
        context = {'menu': menu}
        return render(request, "menu/menu_detail.html", context)
    except Exception as e:
        pass


def orders_list(request):
    """
    Render to list of orders
    """
    start = request.GET.get('start', None)
    end = request.GET.get('end', None)

    try:
        datetime_start = datetime.strptime(start, '%d-%m-%Y') if start else None
        datetime_end = datetime.strptime(end, '%d-%m-%Y') if end else None
    except Exception as e:
        error_mssg = "Error en los formatos de las fechas"
        return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                          exception=RequestError(error_type=ErrorType.ERROR, message=error_mssg))

    if datetime_start and datetime_end:
        orders = Order.objects.filter(created_at__range=[datetime_start, datetime_end]).values('order_id', 'status',
                                                                                               'transaction_number',
                                                                                               'status_pixel',
                                                                                               'status_final',
                                                                                               'store__name',
                                                                                               'store__integrationId',
                                                                                               'aggregator',
                                                                                               'created_at',
                                                                                               'processed_at_pixel',
                                                                                               'aggregator__name',
                                                                                               'mode').order_by(
            '-created_at')
    elif datetime_start:
        orders = Order.objects.filter(created_at__gte=datetime_start).values('order_id', 'status', 'transaction_number',
                                                                             'status_pixel', 'status_final',
                                                                             'store__name',
                                                                             'store__integrationId', 'aggregator',
                                                                             'created_at',
                                                                             'processed_at_pixel', 'aggregator__name',
                                                                             'mode').order_by(
            '-created_at')
    else:
        last_day = datetime.now() - timedelta(days=1)
        orders = Order.objects.filter(created_at__gte=last_day).values('order_id', 'status', 'transaction_number',
                                                                       'status_pixel', 'status_final', 'store__name',
                                                                       'store__integrationId', 'aggregator',
                                                                       'created_at',
                                                                       'processed_at_pixel', 'aggregator__name',
                                                                       'mode').order_by(
            '-created_at')
    context = {'orders': orders,
               'status_canceled': OrderStatusFinalEnum.CANCELED.value,
               'dict_status_rappi': dict_status_rappi,
               'dict_status_pixel': dict_status_pixel,
               'dict_status_final': dict_status_final,
               'dict_mode': dict_mode}
    return render(request, "order/order_list.html", context)


def orders_not_received_list(request):
    """
    Render to list of orders
    """
    dict_stores = {}
    stores = Store.objects.all()
    for store in stores:
        dict_stores[store.id_pk] = store.integrationId

    dict_aggregators = {}
    aggregators = Aggregator.objects.all()
    for aggregator in aggregators:
        dict_aggregators[aggregator.id_pk] = aggregator.name
    orders = OrderNotReceived.objects.all().values('order_id', 'store_id', 'aggregator_id',
                                                   'created_at', 'exception', 'mode').order_by('-created_at')
    context = {'orders': orders,
               'dict_status_rappi': dict_status_rappi,
               'dict_status_pixel': dict_status_pixel,
               'dict_status_final': dict_status_final,
               'dict_mode': dict_mode,
               'dict_aggregators': dict_aggregators,
               'dict_stores': dict_stores}
    return render(request, "order_not_received/order_not_received_list.html", context)


def orders_canceled_list(request):
    """
    Render to list of orders canceled
    """
    # orders = Order.objects.filter(status_final=OrderStatusFinalEnum.CANCELED.value)
    context = {'status_final': OrderStatusFinalEnum.CANCELED.value,
               'dict_status_rappi': dict_status_rappi,
               'dict_status_pixel': dict_status_pixel,
               'dict_status_final': dict_status_final,
               'dict_mode': dict_mode}
    return render(request, "order_canceled/order_canceled_list.html", context)


def user_add(request):
    """
    Allow add a new user
    """
    if request.method == "GET":
        context = {
            'form': CustomUserCreationForm(),
            'create': True}
        return render(request, "user/user_form.html", context)

    elif request.method == "POST":
        form = CustomUserCreationForm(data=request.POST)
        if form.is_valid():
            new_user = form.save()
            new_user.is_active = True
            new_user.is_staff = True if request.POST.get('is_staff', None) else False
            new_user.first_name = request.POST.get('first_name', None)
            new_user.last_name = request.POST.get('last_name', None)
            new_user.email = request.POST.get('email', None)
            new_user.save()
            User = get_user_model()
            users = User.objects.all()
            context = {'users': users}
            return render(request, "user/user_list.html", context)
        else:
            print(form.errors)
            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                              exception=RequestError(error_type=ErrorType.ERROR,
                                                     message="Error en los datos del usuario"))


def user_edit(request, id):
    """
    Allow edit a user
    """
    User = get_user_model()
    user = None
    try:
        user = User.objects.get(id=id)
    except:
        pass

    if not user:
        return BadRequest(http_status_code=status.HTTP_404_NOT_FOUND,
                          exception=RequestError(error_type=ErrorType.ERROR,
                                                 message="No existe un usuario con identificador {}".format(id)))

    if request.method == "GET":
        context = {'form': CustomUserEditionForm(instance=user),
                   'create': False}
        return render(request, "user/user_form.html", context)

    elif request.method == "POST":
        form = CustomUserEditionForm(data=request.POST, instance=user)
        if form.is_valid():
            new_user = form.save()
            new_user.is_active = True if request.POST.get('is_active', None) else False
            new_user.is_staff = True if request.POST.get('is_staff', None) else False
            new_user.first_name = request.POST.get('first_name', None)
            new_user.last_name = request.POST.get('last_name', None)
            new_user.email = request.POST.get('email', None)
            new_user.save()
            User = get_user_model()
            users = User.objects.all()
            context = {'users': users}
            return render(request, "user/user_list.html", context)
        else:
            print(form.errors)
            return BadRequest(http_status_code=status.HTTP_400_BAD_REQUEST,
                              exception=RequestError(error_type=ErrorType.ERROR,
                                                     message="Error en los datos del usuario"))


def user_list(request):
    """
    Allow add a new user
    """
    if request.method == "GET":
        User = get_user_model()
        users = User.objects.all()
        context = {'users': users}
        return render(request, "user/user_list.html", context)


def logout_view(request):
    logout(request)
    return redirect('/login')
