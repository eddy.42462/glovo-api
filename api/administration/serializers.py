from rest_framework import serializers
from store.models import Store, Aggregator


class AggregatorSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model Aggregator
    """

    class Meta:
        model = Aggregator
        fields = ('code', 'name', 'production_url', 'develop_url')


class StoreSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model Store
    """
    aggregator = AggregatorSerializer(many=True, read_only=True)

    class Meta:
        model = Store
        fields = ('integrationId', 'name', 'active', 'aggregator')
