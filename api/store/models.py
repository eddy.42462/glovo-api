from django.db import models
from django.db.models import JSONField

from .enum import ModeEnum


class Aggregator(models.Model):
    id_pk = models.AutoField(primary_key=True)
    code = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=64, unique=True)
    production_url = models.CharField(max_length=256, null=True, blank=True)
    develop_url = models.CharField(max_length=256, null=True, blank=True)

    def __str__(self):
        return self.name


class Store(models.Model):
    id_pk = models.AutoField(primary_key=True)
    integrationId = models.CharField(max_length=64, unique=True)
    name = models.CharField(max_length=64)
    aggregator = models.ManyToManyField('Aggregator', through="StoreAggregator")
    active = models.BooleanField(default=True)
    production_mode = models.BooleanField(default=True)
    develop_mode = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    @property
    def menus(self):
        return self.menu_set.all()

    @property
    def orders(self):
        return self.order_set.all()


class StoreAggregator(models.Model):
    id_pk = models.AutoField(primary_key=True)
    aggregator = models.ForeignKey(Aggregator, on_delete=models.CASCADE)
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    relation_id = models.CharField(max_length=64, null=True)
    production_credentials = JSONField(null=True)
    develop_credentials = JSONField(null=True)


class Token(models.Model):
    id = models.AutoField(primary_key=True)
    store_id = models.PositiveIntegerField()
    token = models.TextField()
    date_expired = models.DateTimeField()
    aggregator = models.CharField(max_length=32)
    mode = models.IntegerField(default=ModeEnum.PRODUCTION.value)


class Trace(models.Model):
    id = models.AutoField(primary_key=True)
    trace_type = models.PositiveIntegerField(null=True)
    instance_id = models.CharField(max_length=64, null=True)
    exception = models.TextField(null=True)
    content = models.TextField(null=True)
    mode = models.IntegerField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
