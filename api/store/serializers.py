from rest_framework import serializers

from .models import *


class AggregatorSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model Aggregator
    """

    class Meta:
        model = Aggregator
        fields = ('code', 'name',)


class StoreSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model Store
    """
    aggregator = AggregatorSerializer(many=True, read_only=True)

    class Meta:
        model = Store
        fields = ('integrationId', 'name', 'aggregator')
