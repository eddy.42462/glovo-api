from enum import Enum


class AggregatorEnum(Enum):
    GLOVO = 'Glovo'
    RAPPI = 'Rappi'
    UBBER = 'Ubber'


class OrderStatusFinalEnum(Enum):
    SUCCESS = 0
    CANCELED = 1


class OrderStatusPixelEnum(Enum):
    CREATED = 0
    SENT = 1
    PROCESSED = 2
    REJECTED = 3


class OrderStatusRappiEnum(Enum):
    CREATED = 0
    READY = 1
    SENT = 2
    TAKEN = 3
    REJECTED = 4
    TIMEOUT = 5
    READY_FOR_PICKUP = 6


class ModeEnum(Enum):
    PRODUCTION = 0
    DEVELOP = 1


class TraceTypeEnum(Enum):
    STORE_AVAILABILITY = 0  # habilitar o deshabilitar una tienda
    MENU_PRODUCT_AVAILABILITY = 1  # habilitar o deshabilitar un producto del menu
    MENU_CREATE_OR_UPDATE = 2  # insertar o actualizar un menu en el agregador
    MENU_STATUS = 3  # obtener el estado de un menu en el agregador
    MENU_ALL_DATA_IN_AGGREGATOR = 4  # obtener el estado de un menu en el agregador

    ORDER_ADD = 5  # cuando se esta registrando una orden
    ORDER_ADD_RESPONSE = 6  # cuando se registra el response de un listado de ordenes recibidas del agregador
    ORDER_CHANGE_STATUS = 7  # cambiar el estado de una orden en el agregador
    ORDER_CANCEL_BY_AGGREGATOR = 8 # funcionalidad que cambia el estado final a cancelada si han pasado mas de 5 minutos
    ORDER_EVENTS = 9  # obtener el listado de eventos de una orden
    GET_NEW_ORDERS = 10 # obtener las nuevas ordenes del agregador

    AUTHENTICATION = 11
    IMAGE = 12
    OTHER = 13
    STORE = 14


class OrderCancelTypeEnum(Enum):
    USER = 0
    AGGREGATOR = 1


class MenuStatusGlovoEnum(Enum):
    SUCCESS = 0
    PROCESSING = 1
    FETCH_MENU_INVALID_PAYLOAD = 2
    FETCH_MENU_SERVER_ERROR = 3
    FETCH_MENU_UNAUTHORIZED = 4
    GLOVO_ERROR = 5


class MenuStatusRappiEnum(Enum):
    SENT = 0
    AVAILABLE = 1
    UNAVAILABLE = 2
