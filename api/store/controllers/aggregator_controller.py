from ..enum import TraceTypeEnum, ModeEnum
from ..models import Aggregator
from ..utils import insert_trace


class AggregatorController:

    def get_aggregator_by_name(self, aggregator_name, trace_type, mode):
        try:
            return Aggregator.objects.get(name=aggregator_name)
        except Exception as e:
            insert_trace(trace_type=trace_type.value,
                         instance_id=aggregator_name,
                         exception=str(e),
                         mode=mode.value)
            return None


    def get_base_url_by_mode(self, mode, aggregator, instace_id, trace_type):
        base_url = None
        if mode == ModeEnum.DEVELOP:
            if not aggregator.develop_url:
                exception = "Aggregator with name {} not have develop_url".format(aggregator.name)
                insert_trace(trace_type=trace_type.value,
                             exception=exception,
                             instance_id=instace_id,
                             mode=mode.value)
            else:
                base_url = aggregator.develop_url
        else:
            if not aggregator.production_url:
                exception = "Aggregator with name {} not have production_url".format(aggregator.name)
                insert_trace(trace_type=trace_type.value,
                             exception=exception,
                             instance_id=instace_id,
                             mode=mode.value)
            else:
                base_url = aggregator.production_url
        return base_url
