from ..enum import TraceTypeEnum, ModeEnum
from ..models import Store, StoreAggregator
from ..utils import insert_trace


class StoreController:

    def get_store_by_store_id(self, store_id, trace_type, mode):
        try:
            return Store.objects.get(integrationId=store_id)
        except Exception as e:
            insert_trace(trace_type=trace_type.value,
                         instance_id=store_id,
                         exception=str(e),
                         mode=mode.value)
            return None

    def store_define_active(self, store, aggregator, trace_type, mode):
        if not store.active:
            exception = "Store with id {} is not active".format(aggregator.name, store.integrationId)
            insert_trace(trace_type=trace_type.value,
                         instance_id=store.integrationId,
                         exception=exception,
                         mode=mode.value)
            return False
        return True

    def store_define_mode_active(self, store, aggregator, trace_type, mode):
        if mode == ModeEnum.DEVELOP and not store.develop_mode:
            exception = "Store with id {} not have DEVELOP mode activate".format(aggregator.name, store.integrationId)
            insert_trace(trace_type=trace_type.value,
                         instance_id=store.integrationId,
                         exception=exception,
                         mode=mode.value)
            return False

        if mode == ModeEnum.PRODUCTION and not store.production_mode:
            exception = "Store with id {} not have PRODUCTION mode activate".format(
                aggregator.name, store.integrationId)
            insert_trace(trace_type=trace_type.value,
                         exception=exception,
                         instance_id=store.integrationId,
                         mode=mode.value)
            return False
        return True

    def get_store_aggregator_relation(self, store, aggregator, trace_type, mode):
        store_aggregator = StoreAggregator.objects.filter(store=store, aggregator=aggregator)

        if not store_aggregator:
            exception = "Store with id {} not related with Aggregator {}".format(store.integrationId, aggregator.name)
            insert_trace(trace_type=trace_type.value,
                         exception=exception,
                         instance_id=store.integrationId,
                         mode=mode.value)
            return None

        if not store_aggregator[0].relation_id:
            exception = "Store with id {} not have relation_id with Aggregator {}".format(store.integrationId,
                                                                                          aggregator.name)
            insert_trace(trace_type=trace_type.value,
                         exception=exception,
                         instance_id=store.integrationId,
                         mode=mode.value)
            return None
        return store_aggregator[0]
