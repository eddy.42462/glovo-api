import json
from datetime import datetime, timedelta

import requests
from django.conf import settings
from rest_framework import status

from .enum import ModeEnum, AggregatorEnum, TraceTypeEnum
from .models import Token, Trace


def get_headers_by_aggregator(aggregator, store, mode, store_aggregator):
    if aggregator == AggregatorEnum.RAPPI.value:
        return {'x-authorization': 'Bearer ' + get_valid_token_by_store_id(aggregator, store, mode, store_aggregator),
                'Content-Type': 'application/json',
                'Accept': 'application/json'}

    elif aggregator == AggregatorEnum.GLOVO.value:
        return {'Authorization': get_valid_token_by_store_id(aggregator, store, mode, store_aggregator),
                'content-type': 'application/json',
                'Accept': 'application/json'}

    else:
        return {}


def get_valid_token_by_store_id(aggregator, store, mode, store_aggregator):
    now = datetime.now() + timedelta(seconds=120)

    tokens = Token.objects.filter(store_id=store.id_pk,
                                  date_expired__gt=now,
                                  aggregator=aggregator,
                                  mode=mode.value)
    if len(tokens) > 0:
        token = tokens[0]
        return token.token

    else:
        if aggregator == AggregatorEnum.RAPPI.value:
            if mode == ModeEnum.PRODUCTION and not store_aggregator.production_credentials:
                return ''

            if mode == ModeEnum.DEVELOP and not store_aggregator.develop_credentials:
                return ''

            headers = {'Content-Type': 'application/json'}

            body_request = store_aggregator.production_credentials if mode == ModeEnum.PRODUCTION else store_aggregator.develop_credentials

            try:
                response = requests.post(settings.API_RAPPI_DEV_OAUTH, data=json.dumps(body_request), headers=headers)
            except Exception as e:
                print(str(e))
                exception = "Error getting token authentication, details: {}".format(e)
                insert_trace(exception=exception,
                             trace_type=TraceTypeEnum.AUTHENTICATION.value,
                             content=str(body_request),
                             mode=mode.value)
                return ''

            if response.status_code == status.HTTP_200_OK:
                now = datetime.now()
                date_expired = now + timedelta(seconds=response.json()['expires_in'])
                token_inserted = None
                data = {'token': response.json().get('access_token', None),
                        'date_expired': date_expired,
                        'aggregator': aggregator,
                        'mode': mode.value}
                try:
                    token_inserted, _ = Token.objects.update_or_create(store_id=store.id_pk,
                                                                       aggregator=aggregator,
                                                                       mode=mode.value,
                                                                       defaults=data)
                except Exception as e:
                    exception = "Error inserting token in db, details:{}".format(e)
                    insert_trace(exception=exception,
                                 trace_type=TraceTypeEnum.AUTHENTICATION.value,
                                 content=str(data),
                                 mode=mode.value)
                if token_inserted:
                    return token_inserted.token
            else:
                exception = "Bad response with status {} getting token authentication, details:{}".format(
                    response.status_code, response.content)
                insert_trace(exception=exception,
                             trace_type=TraceTypeEnum.AUTHENTICATION.value,
                             content=str(body_request),
                             mode=mode.value)
                print('Response Get Token: {}'.format(response.json()))

        return ''


def insert_trace(trace_type=None, instance_id=None, exception=None, content=None, mode=None):
    return Trace.objects.create(**{'trace_type': trace_type,
                                   'instance_id': instance_id,
                                   'exception': exception,
                                   'content': content,
                                   'mode': mode})
