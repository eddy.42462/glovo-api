from .enum import OrderStatusRappiEnum, OrderStatusPixelEnum, OrderStatusFinalEnum, TraceTypeEnum, ModeEnum

dict_status_rappi = {
    OrderStatusRappiEnum.CREATED.value: 'CREATED',
    OrderStatusRappiEnum.READY.value: 'READY',
    OrderStatusRappiEnum.SENT.value: 'SENT',
    OrderStatusRappiEnum.TAKEN.value: 'TAKEN',
    OrderStatusRappiEnum.REJECTED.value: 'REJECTED',
    OrderStatusRappiEnum.TIMEOUT.value: 'TIMEOUT',
    OrderStatusRappiEnum.READY_FOR_PICKUP.value: 'READY_FOR_PICKUP',
}

dict_status_pixel = {
    OrderStatusPixelEnum.CREATED.value: 'CREATED',
    OrderStatusPixelEnum.SENT.value: 'SENT',
    OrderStatusPixelEnum.PROCESSED.value: 'PROCESSED',
    OrderStatusPixelEnum.REJECTED.value: 'REJECTED',
}

dict_status_final = {
    OrderStatusFinalEnum.SUCCESS.value: 'SUCCESS',
    OrderStatusFinalEnum.CANCELED.value: 'CANCELED',
}

dict_mode = {
    ModeEnum.PRODUCTION.value: 'PRODUCTION',
    ModeEnum.DEVELOP.value: 'DEVELOP',
}
