from order.notifications_templates import *
from requests import Response
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from store.enum import ModeEnum, TraceTypeEnum, AggregatorEnum

from .controllers.aggregator_controller import AggregatorController
from .controllers.store_controller import StoreController
from .models import *
from .serializers import StoreSerializer
from .utils import *


class StoreViewSet(viewsets.ModelViewSet):
    """
    Servicios de Store
    """
    queryset = Store.objects.all().order_by('integrationId')
    serializer_class = StoreSerializer

    store_ctl = StoreController()
    aggregator_ctl = AggregatorController()

    @action(detail=False, methods=['get'], url_path='status')
    def service_status(self, request):
        """
        Servicio utilitario que permite conocer el estado del API
        """
        return Response({'status': 'OK'}, status=status.HTTP_200_OK)

    @action(detail=False, methods=['put'],
            url_path='availability/(?P<aggregator_name>[^/.]+)/(?P<store_id>[^/.]+)/(?P<type>[^/.]+)')
    def close_store_temporarily(self, request, aggregator_name, store_id, type):
        """
        Descripcion: Servicio que permite habilitar o deshabilitar una tienda en el agregador especificado por parametro

        Parametros obligatorios (path params):
        "aggregator_name": nombre del agregador
        "store_id"       : identificador de la tienda, ruc + nro_establecimiento
        "type"           : valor 1 o 2, donde 1 es habilitar la tienda y 2 deshabilitarla

        Parametros opcionales (request params):
        develop          : si viene con valor define si se usa el API del agregador en modo produccion o desarrollo, por
        defecto o sea si no se le especifica valor se toma como produccion

        Implementado para los agregadores:
        - Rappi
        - Glovo
        """
        mode = ModeEnum.DEVELOP if request.GET.get('develop', False) else ModeEnum.PRODUCTION

        trace_type = TraceTypeEnum.STORE_AVAILABILITY
        store = self.store_ctl.get_store_by_store_id(store_id, trace_type, mode)
        if not store:
            body = {'message': NOT_EXIST_STORE_ID.format(store_id), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        aggregator = self.aggregator_ctl.get_aggregator_by_name(aggregator_name, trace_type, mode)
        if not aggregator:
            body = {'message': AGGREGATOR_NOT_EXIST.format(aggregator_name), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        if not self.store_ctl.store_define_active(store, aggregator, trace_type, mode):
            body = {'message': NOT_ACTIVE_STORE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        selected_mode_active = self.store_ctl.store_define_mode_active(store, aggregator, trace_type, mode)
        if not selected_mode_active:
            body = {'message': STORE_NOT_ACTIVE_MODE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = StoreAggregator.objects.filter(store=store, aggregator=aggregator)
        if not store_aggregator:
            body = {'message': STORE_NOT_RELATED, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = self.store_ctl.get_store_aggregator_relation(store, aggregator, trace_type, mode)

        base_url = self.aggregator_ctl.get_base_url_by_mode(mode, aggregator, store_id, trace_type)
        if not base_url:
            body = {'message': STORE_NOT_URL_BASE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        headers = get_headers_by_aggregator(aggregator_name, store, mode, store_aggregator)

        if aggregator_name == AggregatorEnum.RAPPI.value:
            url = base_url + '/availability/stores'

            if type == 1:
                body = {'turn_on': [store_id]}
            else:
                body = {'turn_off': [store_id]}

            response = requests.put(url=url, headers=headers, data=body)
            if response.status_code == status.HTTP_200_OK:
                body = {'message': STORE_UPDATE_SUCCESS, 'details': ''}
                return Response(data=body, status=status.HTTP_200_OK)
            else:
                body = {'message': STORE_NOT_UPDATE_SUCCESS, 'details': json.loads(response.content)}
                return Response(data=body, status=response.status_code)


        # # # TODO esta es la implementacion de GLOVO para la misma funcionalidad de cerrar una tienda
        # elif aggregator_name == AggregatorEnum.GLOVO.value:
        #     if type == 1:
        #         try:
        #             validate(instance=request.data,
        #                      schema={"properties": {"until": {"type": "string"}, },
        #                              "required": ["until"]})
        #         except:
        #             return Response({'error': 'Error data structure'},
        #                             status=status.HTTP_400_BAD_REQUEST)
        #
        #         url = settings.API_GLOVO_PRODUCTION_URL + '/webhook/stores/{}/closing'.format(store_id)
        #         response = requests.put(url, headers=get_headers_by_aggregator(aggregator_name), data=request.data)
        #         if response.status_code == status.HTTP_204_NO_CONTENT:
        #             return Response({'message': 'Store with id {} closed successfully'.format(store_id)},
        #                             status=status.HTTP_200_OK)
        #
        #         return Response({'error': 'Store with id {} could not be closed successfully'.format(store_id)},
        #                         status=response.status_code)
        #     else:
        #         url = settings.API_GLOVO_PRODUCTION_URL + '/webhook/stores/{}/closing'.format(store_id)
        #         response = requests.delete(url, headers={'Authorization': settings.GLOVO_ACCESS_TOKEN})
        #         if response.status_code == status.HTTP_204_NO_CONTENT:
        #             return Response(
        #                 {'message': 'Deleted temporary closing successfully from Store with id {}  '.format(store_id)},
        #                 status=status.HTTP_200_OK)
        #         return Response(
        #             {'error': 'Could not be deleted temporary closing from Store with id {}'.format(store_id)},
        #             status=status.HTTP_400_BAD_REQUEST)

        elif aggregator_name == AggregatorEnum.UBBER.value:
            return Response(data={'message': 'Aun no implementado', 'details': ''}, status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response(data={'message': 'Agregador desconocido', 'details': ''},
                            status=status.HTTP_400_BAD_REQUEST)

# @action(detail=False, url_path='schedule/(?P<aggregator_name>[^/.]+)/(?P<store_id>[^/.]+)')
# def store_schedule(self, request, aggregator_name, store_id):
#     """
#     Servicio que permite obtener el horario de trabajo de una tienda
#
#     Descripcion:
#     """
#     try:
#         store = Store.objects.get(integrationId=store_id)
#     except:
#         return Response({'error': 'Store with id {} do not exist'.format(store_id)},
#                         status=status.HTTP_404_NOT_FOUND)
#
#     if AggregatorEnum.GLOVO.value in store.aggregator.values_list('name', flat=True):
#         url = settings.API_GLOVO_PRODUCTION_URL + '/webhook/stores/{}/schedule'.format(store_id)
#         response = requests.get(url, headers={'Authorization': settings.GLOVO_ACCESS_TOKEN})
#         if response.status_code == status.HTTP_200_OK:
#             return Response(json.loads(response.content), status=response.status_code)
#         return Response({'error': 'Schedule for Store with id {} could not be obtained'.format(store_id)},
#                         status=response.status_code)
#
#     else:
#         return Response({'error': 'Store with id {} is not related to the {} aggregator'.format(store_id,
#                                                                                                 settings.AGGREGATORS[
#                                                                                                     0])},
#                         status=status.HTTP_400_BAD_REQUEST)

# @action(detail=False, url_path='date/closed/(?P<store_id>[^/.]+)')
# def get_temporarily_close_store(self, request, store_id):
#     """
#     Allows close temporarily a Store
#     """
#     try:
#         store = Store.objects.get(integrationId=store_id)
#     except:
#         return Response({'error': 'Store with id {} do not exist'.format(store_id)},
#                         status=status.HTTP_404_NOT_FOUND)
#
#     if AggregatorEnum.GLOVO.value in store.aggregator.values_list('name', flat=True):
#         url = settings.API_GLOVO_PRODUCTION_URL + '/webhook/stores/{}/closing'.format(store_id)
#         response = requests.get(url, headers={'Authorization': settings.GLOVO_ACCESS_TOKEN})
#         return Response(json.loads(response.content), status=response.status_code)
#
#     else:
#         return Response({'error': 'Store with id {} is not related to the {} aggregator'.format(store_id,
#                                                                                                 settings.AGGREGATORS[
#                                                                                                     0])},
#                         status=status.HTTP_400_BAD_REQUEST)
#

# @action(detail=False, url_path='list/client')
# def get_stores_by_client(self, request):
#     """
#     Allows get list of Stores for the authenticated client
#     """
#     url = settings.API_RAPPI_URL + '/stores-pa'
#     response = requests.get(url, headers=get_headers_by_aggregator(AggregatorEnum.GLOVO.value))
#     return Response(json.loads(response.content), status=response.status_code)
