# Generated by Django 3.1 on 2021-05-31 23:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0004_auto_20210530_1801'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='trace',
            name='action_type',
        ),
        migrations.AddField(
            model_name='trace',
            name='instance_id',
            field=models.CharField(max_length=64, null=True),
        ),
    ]
