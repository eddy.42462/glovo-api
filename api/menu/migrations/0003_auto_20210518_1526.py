# Generated by Django 3.1 on 2021-05-18 15:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('menu', '0002_menu_mode'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='menu',
            name='mode',
        ),
        migrations.AddField(
            model_name='menu',
            name='sent_develop',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='menu',
            name='sent_production',
            field=models.BooleanField(default=False),
        ),
    ]
