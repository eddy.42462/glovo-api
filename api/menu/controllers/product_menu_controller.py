from ..models import ProductMenu
from store.enum import TraceTypeEnum
from store.utils import insert_trace


class ProductMenuController:

    def insert_product_rappi(self, products_data, menu, mode):
        products_dict = {}
        for product_data in products_data:
            new_products_dict = self.prepare_product(product_added=products_dict, data=product_data, menu=menu)
            if new_products_dict:
                products_dict.update(new_products_dict)

        products_to_insert = []
        for (key, value) in products_dict.items():
            products_to_insert.append(value)

        try:
            ProductMenu.objects.bulk_create(products_to_insert)
        except Exception as e:
            exception = "Can not insert products of the Menu with id {}, details: {}".format(menu.menu_id, e)
            insert_trace(trace_type=TraceTypeEnum.MENU_CREATE_OR_UPDATE.value,
                         exception=exception,
                         instance_id=menu.menu_id,
                         content=str(products_to_insert),
                         mode=mode.value)

    def prepare_product(self, product_added, data, menu):
        product_id = data['sku']
        if product_id not in product_added:
            max_limit = int(data.get('maxLimit')) if data.get('maxLimit') else None
            product_added.update({product_id: ProductMenu(id=product_id,
                                                          name=data.get('name', None),
                                                          price=str(data.get('price', '')),
                                                          image_url=data.get('imageUrl', None),
                                                          description=data.get('description', None),
                                                          available=data.get('available', None),
                                                          rappi_ids=data.get('rappiIds', []),
                                                          sku=product_id,
                                                          menu=menu,
                                                          stock=data.get('stock', None),
                                                          sorting_position=data.get('sortingPosition', None),
                                                          type=data.get('type', None),
                                                          max_limit=max_limit)})

        products_data = data.pop('children', [])
        for product_data in products_data:
            product_added = self.prepare_product(product_added=product_added, data=product_data, menu=menu)
        return product_added
