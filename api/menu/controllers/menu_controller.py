from ..models import Menu
from ..serializers import MenuSerializer
from store.enum import TraceTypeEnum, ModeEnum
from store.utils import insert_trace


class MenuController:

    def insert_menu(self, menu_id, store, aggregator, menu_data, mode):
        serializer_data = {
            'menu_id': menu_id,
            'store_id': store.id_pk,
            'aggregator_id': aggregator.id_pk,
            'menu_json': menu_data,
            'sent_production': True if mode == ModeEnum.PRODUCTION else False,
            'sent_develop': True if mode == ModeEnum.DEVELOP else False
        }

        menu_serializer = MenuSerializer(data=serializer_data)
        if menu_serializer.is_valid():
            return menu_serializer.save()
        else:
            insert_trace(trace_type=TraceTypeEnum.MENU_CREATE_OR_UPDATE.value,
                         exception=str(menu_serializer.errors),
                         content=str(menu_data),
                         mode=mode)
        return None

    def update_sent_mode(self, menu_id, mode):
        defaults_dict = {}
        if mode == ModeEnum.PRODUCTION:
            defaults_dict['sent_production'] = True
        else:
            defaults_dict['sent_develop'] = True
        menu_updated, _ = Menu.objects.update_or_create(menu_id=menu_id, defaults=defaults_dict)
        return menu_updated
