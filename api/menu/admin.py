from django.contrib import admin

from .models import *

admin.site.register(Menu)
admin.site.register(Image)
admin.site.register(ProductMenu)
