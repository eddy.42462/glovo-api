import io
import traceback

from PIL import Image as ImagePil


def convert_image(image_name, image, object_format):
    try:
        image_converted = image.convert('RGB')
        image_converted.save('images/' + image_name, object_format)
        return image_converted
    except Exception:
        traceback.print_exc()


def resize_image(image_name, image_format, image, weight_permit, weight_actual):
    try:
        print(str(weight_actual) + '/' + str(weight_permit))
        po = weight_actual / weight_permit + 0.3
        width, height = image.size
        new_width, new_height = round(width / po), round(height / po)
        # new_width, new_height = round(width - (width * 0.50)), round(height - (height * 0.50))
        image = image.resize((new_width, new_height))
        image.save('images/' + image_name, image_format)
        byte_array = image.tobytes()
        wa = len(byte_array) / 1024 / 1024
        print('{} Peso actual: {} mb'.format(image_format, wa))
        if wa > weight_permit:
            image = resize_image(image_name, image_format, image, weight_permit, wa)
            return image
        return image
    except Exception:
        traceback.print_exc()


def resize_image2(image_name, byte_array, weight_permit, weight_actual):
    try:
        basewidth = 900
        image = ImagePil.open(io.BytesIO(byte_array))

        wpercent = (basewidth / float(image.size[0]))
        hsize = int((float(image.size[1]) * float(wpercent)))

        # po = weight_actual / weight_permit + 0.5
        # img_file =
        # d = img_file.tell()
        img = image.resize((basewidth, hsize), ImagePil.ANTIALIAS)

        # width, height = image.size
        # new_image = image.resize((round(width / po), round(height / po)))
        img.save(image_name + '.jpg', 'JPEG')
        print('Antes: {} mb'.format(len(byte_array) / 1024 / 1024))
        new_byte_array = img.tobytes()
        wa = len(new_byte_array) / 1024 / 1024
        print('Despues: {} mb'.format(wa))

        return new_byte_array
    except Exception:
        traceback.print_exc()
