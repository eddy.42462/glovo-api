import json

import requests
from order.notifications_templates import *
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from store.controllers.aggregator_controller import AggregatorController
from store.controllers.store_controller import StoreController
from store.enum import AggregatorEnum, ModeEnum, TraceTypeEnum
from store.models import StoreAggregator
from store.utils import get_headers_by_aggregator
from store.utils import insert_trace

from ..models import ProductMenu
from ..serializers import ProductMenuSerializer


class ProductMenuViewSet(viewsets.ModelViewSet):
    """
    Servicios de ProductMenu
    """
    queryset = ProductMenu.objects.all().order_by('name')
    serializer_class = ProductMenuSerializer

    aggregator_ctl = AggregatorController()
    store_ctl = StoreController()

    @action(detail=False, methods=['put'],
            url_path='availability/items/sku/(?P<aggregator_name>[^/.]+)/(?P<store_id>[^/.]+)')  # TODO OJO EN USO
    def change_availability_items_by_sku(self, request, aggregator_name, store_id):
        """
        Descripcion: Servicio que permite habilitar o deshabilitar un producto de un menu en su agregador

        Parametros obligatorios (path params):
        "aggregator_name" : nombre del agregador
        "store_id"        : identificador de la tienda, ruc + nro_establecimiento

        Parametros opcionales (request params):
        develop          : si viene con valor define si se reliza la actualizacion del producto en el API de produccion o
        desarrollo del agregador, por defecto toma el API de produccion

        Implementado para los agregadores:
        - Rappi
        """
        data = request.data
        mode = ModeEnum.DEVELOP if request.GET.get('develop', False) else ModeEnum.PRODUCTION

        trace_type = TraceTypeEnum.MENU_PRODUCT_AVAILABILITY
        store = self.store_ctl.get_store_by_store_id(store_id, trace_type, mode)
        if not store:
            body = {'message': NOT_EXIST_STORE_ID.format(store_id), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        aggregator = self.aggregator_ctl.get_aggregator_by_name(aggregator_name, trace_type, mode)
        if not aggregator:
            body = {'message': AGGREGATOR_NOT_EXIST.format(aggregator_name), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        if not self.store_ctl.store_define_active(store, aggregator, trace_type, mode):
            body = {'message': NOT_ACTIVE_STORE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        selected_mode_active = self.store_ctl.store_define_mode_active(store, aggregator, trace_type, mode)
        if not selected_mode_active:
            body = {'message': STORE_NOT_ACTIVE_MODE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = StoreAggregator.objects.filter(store=store, aggregator=aggregator)
        if not store_aggregator:
            body = {'message': STORE_NOT_RELATED, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = self.store_ctl.get_store_aggregator_relation(store, aggregator, trace_type, mode)

        base_url = self.aggregator_ctl.get_base_url_by_mode(mode, aggregator, store_id, trace_type)
        if not base_url:
            body = {'message': STORE_NOT_URL_BASE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        headers = get_headers_by_aggregator(aggregator_name, store, mode, store_aggregator)

        if aggregator_name == AggregatorEnum.RAPPI.value:
            data['store_integration_id'] = store_aggregator.relation_id
            url = base_url + '/availability/stores/items'

            try:
                response = requests.put(url=url, headers=headers, data=json.dumps([data]))
            except Exception as e:
                print(str(e))
                exception = "Products {} availability not changed, details: {}".format(data, e)
                insert_trace(trace_type=TraceTypeEnum.MENU_PRODUCT_AVAILABILITY.value,
                             exception=exception,
                             content=str([data]),
                             mode=mode.value)
                body = {'message': PRODUCT_NOT_UPDATE_SUCCESS, 'details': ''}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            if response.status_code == status.HTTP_200_OK:

                exception = "Products availability updated in Aggregator {} success".format(aggregator_name)
                insert_trace(trace_type=TraceTypeEnum.MENU_PRODUCT_AVAILABILITY.value,
                             exception=exception,
                             content=str([data]),
                             mode=mode.value)

                body = {'message': PRODUCT_UPDATE_SUCCESS, 'details': ''}
                return Response(data=body, status=status.HTTP_200_OK)
            else:
                exception = "Products {} availability not changed because bad status response received from Aggregator {}, details: {}".format(
                    data, aggregator.name, response.content)
                insert_trace(trace_type=TraceTypeEnum.MENU_PRODUCT_AVAILABILITY.value,
                             exception=exception,
                             content=str([data]),
                             mode=mode.value)
                body = {'message': PRODUCT_NOT_UPDATE_SUCCESS, 'details': str(response.content)}
                return Response(data=body, status=response.status_code)

        elif aggregator_name == AggregatorEnum.UBBER.value:
            return Response(data={'message': 'Aun no implementado', 'details': ''}, status=status.HTTP_400_BAD_REQUEST)

        elif aggregator_name == AggregatorEnum.GLOVO.value:
            return Response(data={'message': 'Aun no implementado', 'details': ''}, status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response(data={'message': 'Agregador desconocido', 'details': ''},
                            status=status.HTTP_400_BAD_REQUEST)

    # @action(detail=False, methods=['patch'],
    #         url_path='update/(?P<store_id>[^/.]+)/(?P<product_id>[^/.]+)')
    # def product_aggregator_update(self, request, store_id, product_id):
    #     """
    #     Allow update product data in the aggregator
    #     """
    #     try:
    #         store = Store.objects.get(integrationId=store_id)
    #     except:
    #         return Response(data={'message': NOT_EXIST_STORE_ID.format(store_id)},
    #                         status=status.HTTP_400_BAD_REQUEST)
    #
    #     if AggregatorEnum.GLOVO.value in store.aggregator.values_list('name', flat=True):
    #         url = settings.API_GLOVO_PRODUCTION_URL + '/webhook/stores/{}/products/{}'.format(store_id, product_id)
    #         response = requests.patch(url, headers=get_headers_by_aggregator(AggregatorEnum.GLOVO.value),
    #                                   data=request.data)
    #         if response.status_code == status.HTTP_200_OK:
    #             self.update_all_product_menu(json.loads(response.content))
    #             return Response(response.content, status=status.HTTP_200_OK)
    #         else:
    #             return Response({'error': 'Product could not be updated in the aggregator'},
    #                             status=status.HTTP_400_BAD_REQUEST)
    #
    #
    #     else:
    #         return Response({'error': 'Store with id {} is not related to the {} aggregator!'.format(store_id,
    #                                                                                                  settings.AGGREGATORS[
    #                                                                                                      0])},
    #                         status=status.HTTP_400_BAD_REQUEST)
    #
    # @action(detail=False, methods=['patch'],
    #         url_path='attribute/update/(?P<store_id>[^/.]+)/(?P<attribute_id>[^/.]+)')
    # def attribute_aggregator_update(self, request, store_id, attribute_id):
    #     """
    #     Allow update attribute data in the aggregator
    #     """
    #     try:
    #         store = Store.objects.get(integrationId=store_id)
    #     except:
    #         return Response({'error': 'Store with id {} do not exist!'.format(store_id)},
    #                         status=status.HTTP_400_BAD_REQUEST)
    #
    #     if AggregatorEnum.GLOVO.value in store.aggregator.values_list('name', flat=True):
    #         url = settings.API_GLOVO_PRODUCTION_URL + '/webhook/stores/{}/attributes/{}'.format(store_id, attribute_id)
    #         response = requests.patch(url, headers=get_headers_by_aggregator(AggregatorEnum.GLOVO.value),
    #                                   data=request.data)
    #         if response.status_code == status.HTTP_200_OK:
    #             return Response(response.content, status=status.HTTP_200_OK)
    #         else:
    #             return Response({'error': 'Attribute could not be updated in the aggregator'},
    #                             status=status.HTTP_400_BAD_REQUEST)
    #
    #
    #     else:
    #         return Response({'error': 'Store with id {} is not related to the {} aggregator!'.format(store_id,
    #                                                                                                  settings.AGGREGATORS[
    #                                                                                                      0])},
    #                         status=status.HTTP_400_BAD_REQUEST)
    #
    # def update_all_product_menu(self, product_menu_data):
    #     products_menu = ProductMenu.objects.filter(id=product_menu_data.get('id', None))
    #     for product_menu in products_menu:
    #         product_menu.name = product_menu_data['name']
    #         product_menu.price = product_menu_data['price']
    #         product_menu.image_url = product_menu_data['image_url']
    #         product_menu.description = product_menu_data['description']
    #         product_menu.available = product_menu_data['available']
    #         product_menu.save()
