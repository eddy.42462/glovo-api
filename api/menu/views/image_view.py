from django.conf import settings
from django.http import HttpResponse
from order.notifications_templates import *
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework_xml.renderers import XMLRenderer
from store.utils import insert_trace
from store.models import Store, Aggregator
from store.enum import ModeEnum, TraceTypeEnum, AggregatorEnum, OrderStatusRappiEnum, \
    OrderStatusPixelEnum, OrderStatusFinalEnum, OrderCancelTypeEnum

from ..models import Image, Menu
from ..serializers import BulkImageSerializer
from ..utils import *


class ImageViewSet(viewsets.ModelViewSet):
    """

    """
    parser_classes = (MultiPartParser, FormParser)
    serializer_class = BulkImageSerializer
    renderer_classes = [JSONRenderer, XMLRenderer]

    def get_serializer(self, *args, **kwargs):
        if isinstance(kwargs.get("data", {}), list):
            kwargs["many"] = True
        return super(ImageViewSet, self).get_serializer(
            *args, **kwargs
        )

    def get_queryset(self, ids=None):
        if ids:
            return Image.objects.filter(
                product_id__in=ids,
            )
        return Image.objects.all()

    @action(detail=False, url_path='(?P<menu_id>[^/.]+)/(?P<product_id>[^/.]+)')
    def get_image_by_product_id(self, request, menu_id, product_id):
        """
        Descripcion: Servicio que retorna una la imagen del producto y menu especificados por parametro

        Parametros obligatorios (path params):
        "menu_id"       : identificador del menu
        "product_id"    : identificador del producto

        Parametros opcionales (request params):
        """
        try:
            image = Image.objects.get(product_id=product_id, menu_id=menu_id)
            content_type = 'image/' + image.format if image.format else 'jpg'
            return HttpResponse(image.image_byte, content_type=content_type)
        except Exception as e:
            exception = 'Error getting image from Menu with id {} product_id {}, details: {}'.format(menu_id,
                                                                                                     product_id, e)
            insert_trace(exception=exception,
                         instance_id=product_id,
                         trace_type=TraceTypeEnum.IMAGE.value)
            return HttpResponse('/images/default/default.png', content_type="image/png")

    @action(detail=False, methods=['put'], url_path='update/all/(?P<store_id>[^/.]+)/(?P<menu_id>[^/.]+)')
    def update_or_create_all_images(self, request, store_id, menu_id):
        """
        Descripcion: Servicio que permite insertar o actualizar todas las imagenes de los productos de un menu

        Parametros obligatorios (path params):
        "store_id"       : identificador de la tienda, ruc + nro_establecimiento
        "menu_id"        : identificador del menu

        Parametros opcionales (request params):
        """
        try:
            try:
                store = Store.objects.get(integrationId=store_id)
            except Exception as e:
                exception = "Not created or updated images of the Menu with id {} because Store with id {} do not exist, details: {}".format(
                    menu_id, store_id, e)
                insert_trace(trace_type=TraceTypeEnum.IMAGE.value,
                             exception=exception,
                             instance_id=menu_id)
                body = {'message': NOT_EXIST_STORE_ID.format(store_id), 'details': str(e)}
                return Response(data=body, status=status.HTTP_404_NOT_FOUND)

            try:
                menu = Menu.objects.get(menu_id=menu_id, store=store)
            except Exception as e:
                exception = "Not created or updated images of the Menu with id {} and store {} because menu do no exist, details: {}".format(
                    menu_id, store_id, e)
                insert_trace(trace_type=TraceTypeEnum.IMAGE.value,
                             exception=exception,
                             instance_id=menu_id)
                body = {'message': NOT_EXIST_MENU_ID.format(menu_id), 'details': str(e)}
                return Response(data=body, status=status.HTTP_404_NOT_FOUND)

            if not store.active:
                exception = "Not created or updated images of the Menu with id {} because Store {} is not active".format(
                    menu_id, store_id)
                insert_trace(trace_type=TraceTypeEnum.IMAGE.value,
                             exception=exception,
                             instance_id=menu_id)
                body = {'message': NOT_ACTIVE_STORE, 'details': ''}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            try:
                Image.objects.filter(menu=menu).delete()
            except Exception as e:
                exception = "Not created or updated images of the Menu with id {} because errors deleting all images, details: {}".format(
                    menu_id, e)
                insert_trace(trace_type=TraceTypeEnum.IMAGE.value,
                             exception=exception,
                             instance_id=menu_id)
                body = {'message': UPDATE_MENU_IMAGES_NOT_SUCCESS, 'details': str(e)}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            images = []

            if not request.data.get('images_files', None):
                body = {'message': 'Ninguna imagen actualizada', 'details': ''}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            for image_file in dict(request.data.lists())['images_files']:
                try:
                    product_id = image_file.name.split('.')[0]
                except:
                    product_id = image_file.name

                byte_array = image_file.file.read()
                image = ImagePil.open(io.BytesIO(byte_array))
                image_format = image.format
                # print('-' * 50)
                # print('Imagen: {}'.format(image_file.name))
                format = None

                if image_format not in ['JPEG', 'PNG', 'JPEG 2000']:
                    # print('Convirtiendo imagen {} de {} a .jpg'.format(product_id, image.format))
                    image = convert_image(product_id, image, 'JPEG')
                    byte_array = image.tobytes()
                    format = 'jpg'
                    # print('La imagen convertida pesa ahora {}'.format(len(byte_array) / 1024 / 1024))

                weight_actual = len(byte_array) / 1024 / 1024
                if weight_actual > 2.0:
                    # print('La imagen {} pesa {} mas de los 2.0 mb permitidos'.format(product_id, weight_actual))
                    image = resize_image(product_id, image_format, image, settings.MAX_IMAGE_WEIGHT, weight_actual)
                    byte_array = image.tobytes()

                if not format:
                    format = 'jpg' if image_format in ['JPEG', 'JPEG 2000'] else None
                if not format:
                    format = 'png' if image_format == 'PNG' else None

                images.append(Image(product_id=product_id, image_byte=byte_array, menu=menu, format=format))

            try:
                Image.objects.bulk_create(images)
            except Exception as e:
                exception = "Not created or updated images of the Menu with id {} because errors inserting all images, details: {}".format(
                    menu_id, e)
                insert_trace(trace_type=TraceTypeEnum.IMAGE.value,
                             exception=exception,
                             instance_id=menu_id)
                body = {'message': UPDATE_MENU_IMAGES_NOT_SUCCESS, 'details': str(e)}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

            body = {'message': UPDATE_MENU_IMAGES_SUCCESS, 'details': ''}
            return Response(data=body, status=status.HTTP_200_OK)
        except Exception as e:
            exception = "Not created or updated images of the Menu with id {}, details: {}".format(menu_id, e)
            insert_trace(trace_type=TraceTypeEnum.IMAGE.value,
                         exception=exception,
                         instance_id=menu_id)
            return Response({'message': UPDATE_MENU_IMAGES_NOT_SUCCESS},
                            status=status.HTTP_400_BAD_REQUEST)

    def perform_update(self, serializer):
        serializer.save()
