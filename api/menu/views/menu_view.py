import json

import requests
from order.notifications_templates import *
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from store.controllers.aggregator_controller import AggregatorController
from store.controllers.store_controller import StoreController
from store.enum import ModeEnum, AggregatorEnum, TraceTypeEnum
from store.models import StoreAggregator
from store.utils import get_headers_by_aggregator, insert_trace

from ..controllers.menu_controller import MenuController
from ..controllers.product_menu_controller import ProductMenuController
from ..models import Menu, ProductMenu
from ..serializers import MenuSerializer


class MenuViewSet(viewsets.ModelViewSet):
    """
    """
    queryset = Menu.objects.all().order_by('menu_id')
    serializer_class = MenuSerializer

    store_ctl = StoreController()
    aggregator_ctl = AggregatorController()
    menu_ctl = MenuController()
    product_menu_ctl = ProductMenuController()

    @action(detail=False, methods=['post'],
            url_path='add/(?P<aggregator_name>[^/.]+)/(?P<store_id>[^/.]+)/(?P<menu_id>[^/.]+)')
    def create_or_update_all_menu_data(self, request, aggregator_name, store_id, menu_id):
        """
        Descripcion: Servicio que inserta o actualiza el menu recibido en el agregador especificado

        Parametros obligatorios (path params):
        "aggregator_name": nombre del agregador al cual pertenece el menu
        "store_id"       : identificador de la tienda, ruc + nro_establecimiento
        "menu_id"       : identificador del menu

        Parametros opcionales (request params):
        develop: si viene con valor define si se va a insertar o actualizar el menu en el API de desarrollo o
        produccion, por defecto toma el API de produccion

        Implementado para los agregadores:
        - Rappi
        """
        mode = ModeEnum.DEVELOP if request.GET.get('develop', False) else ModeEnum.PRODUCTION
        request_data = request.data

        trace_type = TraceTypeEnum.MENU_CREATE_OR_UPDATE
        store = self.store_ctl.get_store_by_store_id(store_id, trace_type, mode)
        if not store:
            body = {'message': NOT_EXIST_STORE_ID.format(store_id), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        aggregator = self.aggregator_ctl.get_aggregator_by_name(aggregator_name, trace_type, mode)
        if not aggregator:
            body = {'message': AGGREGATOR_NOT_EXIST.format(aggregator_name), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        if not self.store_ctl.store_define_active(store, aggregator, trace_type, mode):
            body = {'message': NOT_ACTIVE_STORE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        selected_mode_active = self.store_ctl.store_define_mode_active(store, aggregator, trace_type, mode)
        if not selected_mode_active:
            body = {'message': STORE_NOT_ACTIVE_MODE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = StoreAggregator.objects.filter(store=store, aggregator=aggregator)
        if not store_aggregator:
            body = {'message': STORE_NOT_RELATED, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = self.store_ctl.get_store_aggregator_relation(store, aggregator, trace_type, mode)

        base_url = self.aggregator_ctl.get_base_url_by_mode(mode, aggregator, store_id, trace_type)
        if not base_url:
            body = {'message': STORE_NOT_URL_BASE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        headers = get_headers_by_aggregator(aggregator_name, store, mode, store_aggregator)

        if aggregator_name == AggregatorEnum.RAPPI.value:
            return self.send_menu_rappi(request_data, aggregator, store, store_aggregator, menu_id, mode, base_url,
                                        headers, trace_type)

        elif aggregator_name == AggregatorEnum.UBBER.value:
            return Response(data={'message': 'Aun no implementado', 'details': ''}, status=status.HTTP_400_BAD_REQUEST)

        elif aggregator_name == AggregatorEnum.GLOVO.value:
            return Response(data={'message': 'Aun no implementado', 'details': ''}, status=status.HTTP_400_BAD_REQUEST)

        else:
            return Response(data={'message': 'Agregador desconocido', 'details': ''},
                            status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, url_path='status/(?P<aggregator_name>[^/.]+)/(?P<store_id>[^/.]+)/(?P<menu_id>[^/.]+)',
            url_name='status')
    def menu_status(self, request, aggregator_name, store_id, menu_id):
        """
        Descripcion: Servicio que permite obtener el estado de aprobacion de un menu en el agregador especificado

        Parametros obligatorios (path params):
        "aggregator_name": nombre del agregador al cual pertenece el menu
        "store_id"       : identificador de la tienda, ruc + nro_establecimiento
        "menu_id"        : identificador del menu

        Parametros opcionales (request params):
        develop: si viene con valor define si se va a insertar o actualizar el menu en el API de desarrollo o
        produccion, por defecto toma el API de produccion

        Implementado para los agregadores:
        - Rappi
        - Glovo
        """
        mode = ModeEnum.DEVELOP if request.GET.get('develop', False) else ModeEnum.PRODUCTION

        trace_type = TraceTypeEnum.MENU_STATUS
        store = self.store_ctl.get_store_by_store_id(store_id, trace_type, mode)
        if not store:
            body = {'message': NOT_EXIST_STORE_ID.format(store_id), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        aggregator = self.aggregator_ctl.get_aggregator_by_name(aggregator_name, trace_type, mode)
        if not aggregator:
            body = {'message': AGGREGATOR_NOT_EXIST.format(aggregator_name), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        if not self.store_ctl.store_define_active(store, aggregator, trace_type, mode):
            body = {'message': NOT_ACTIVE_STORE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        selected_mode_active = self.store_ctl.store_define_mode_active(store, aggregator, trace_type, mode)
        if not selected_mode_active:
            body = {'message': STORE_NOT_ACTIVE_MODE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = StoreAggregator.objects.filter(store=store, aggregator=aggregator)
        if not store_aggregator:
            body = {'message': STORE_NOT_RELATED, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = self.store_ctl.get_store_aggregator_relation(store, aggregator, trace_type, mode)

        base_url = self.aggregator_ctl.get_base_url_by_mode(mode, aggregator, store_id, trace_type)
        if not base_url:
            body = {'message': STORE_NOT_URL_BASE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        headers = get_headers_by_aggregator(aggregator_name, store, mode, store_aggregator)

        menus = Menu.objects.filter(aggregator=aggregator, store=store, menu_id=menu_id)

        if len(menus) == 1:
            if aggregator_name == AggregatorEnum.RAPPI.value:
                url = base_url + '/menu/approved/{}'.format(store_aggregator.relation_id)
                response = requests.get(url=url, headers=headers)

                if response.status_code == status.HTTP_200_OK:
                    return Response(data={'status': response.text}, status=response.status_code)
                else:
                    exception = "It was not possible to get the status of the Menu with id {} because bad status response received from Aggregator {}, details: {}".format(
                        menu_id, aggregator_name, str(response.content))
                    insert_trace(trace_type=trace_type.value,
                                 instance_id=menu_id,
                                 exception=exception,
                                 mode=mode.value)
                    body = {'status': 'Desconocido'}
                    return Response(data=body, status=response.status_code)

            # elif aggregator_name == AggregatorEnum.GLOVO.value:
            #     if not menu.transaction_id:
            #         body = {'message': 'El menu no tiene id de transaccion'}
            #         return Response(data=body, status=status.HTTP_400_BAD_REQUEST)
            #
            #     url = base_url + '/webhook/stores/{}/menu/{}'.format(store_id, menu.transaction_id)
            #     response = requests.get(url, headers={'Authorization': settings.GLOVO_ACCESS_TOKEN})
            #
            #     if response.status_code == status.HTTP_200_OK:
            #         response_body = response.content
            #         menu.transaction_id = response_body.get('transaction_id', None)
            #         menu.status = response_body.get('status', None)
            #         menu.save()
            #         return Response(response_body, status=status.HTTP_200_OK)
            #     else:
            #         body = {'message': NOT_GET_MENU_STATUS, 'details': json.loads(response.content)}
            #         return Response(data=body, status=response.status_code)

            else:
                body = {'message': 'Agregador desconocido', 'details': ''}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)
        if len(menus) == 0:
            body = {'message': NOT_EXIST_MENU_BY_PARAMS, 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)
        else:
            body = {'message': MANY_EXIST_MENU_BY_PARAMS, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=False, url_path='all/data/(?P<aggregator_name>[^/.]+)/(?P<store_id>[^/.]+)')
    def all_data_in_aggregator(self, request, aggregator_name, store_id, menu_id):
        """
        Descripcion: Servicio que retorna todos los datos de un menu siendo buscados directamente al agregador al cual
        pertenece.

        Parametros obligatorios (path params):
        "aggregator_name": nombre del agregador al cual pertenece el menu
        "store_id"       : identificador de la tienda, ruc + nro_establecimiento
        "menu_id"        : identificador del menu

        Parametros opcionales (request params):
        develop: si viene con valor define si se va a insertar o actualizar el menu en el API de desarrollo o
        produccion, por defecto toma el API de produccion

        Implementado para los agregadores:
        - Rappi
        """
        mode = ModeEnum.DEVELOP if request.GET.get('develop', False) else ModeEnum.PRODUCTION

        trace_type = TraceTypeEnum.MENU_ALL_DATA_IN_AGGREGATOR
        store = self.store_ctl.get_store_by_store_id(store_id, trace_type, mode)
        if not store:
            body = {'message': NOT_EXIST_STORE_ID.format(store_id), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        aggregator = self.aggregator_ctl.get_aggregator_by_name(aggregator_name, trace_type, mode)
        if not aggregator:
            body = {'message': AGGREGATOR_NOT_EXIST.format(aggregator_name), 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)

        if not self.store_ctl.store_define_active(store, aggregator, trace_type, mode):
            body = {'message': NOT_ACTIVE_STORE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        selected_mode_active = self.store_ctl.store_define_mode_active(store, aggregator, trace_type, mode)
        if not selected_mode_active:
            body = {'message': STORE_NOT_ACTIVE_MODE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = StoreAggregator.objects.filter(store=store, aggregator=aggregator)
        if not store_aggregator:
            body = {'message': STORE_NOT_RELATED, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        store_aggregator = self.store_ctl.get_store_aggregator_relation(store, aggregator, trace_type, mode)

        base_url = self.aggregator_ctl.get_base_url_by_mode(mode, aggregator, store_id, trace_type)
        if not base_url:
            body = {'message': STORE_NOT_URL_BASE, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        headers = get_headers_by_aggregator(aggregator_name, store, mode, store_aggregator)

        menus = Menu.objects.filter(aggregator=aggregator, store=store, menu_id=menu_id)

        if len(menus) == 1:
            if aggregator_name == AggregatorEnum.RAPPI.value:
                url = base_url + '/menu/rappi/{}'.format(store_id)
                response = requests.get(url=url, headers=headers)

                if response.status_code == status.HTTP_200_OK:
                    return Response(data=json.loads(response.content), status=response.status_code)
                else:
                    exception = "It was not possible to get the data of the Menu with id {} because bad status response received from Aggregator {}, details: {}".format(
                        menu_id, aggregator_name, str(response.content))
                    insert_trace(trace_type=trace_type.value,
                                 instance_id=menu_id,
                                 exception=exception,
                                 mode=mode.value)
                    body = {'message': NOT_GET_MENU_STATUS, 'details': json.loads(response.content)}
                    return Response(data=body, status=response.status_code)
            else:
                body = {'message': 'Agregador desconocido', 'details': ''}
                return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

        if len(menus) == 0:
            body = {'message': NOT_EXIST_MENU_BY_PARAMS, 'details': ''}
            return Response(data=body, status=status.HTTP_404_NOT_FOUND)
        else:
            body = {'message': MANY_EXIST_MENU_BY_PARAMS, 'details': ''}
            return Response(data=body, status=status.HTTP_400_BAD_REQUEST)

    def send_menu_rappi(self, request_data, aggregator, store, store_aggregator, menu_id, mode, base_url, headers,
                        trace_type):
        request_data['storeId'] = store_aggregator.relation_id
        menu = None
        menus = Menu.objects.filter(aggregator=aggregator, store=store, menu_id=menu_id)

        if len(menus) == 0:
            menu = self.menu_ctl.insert_menu(menu_id, store, aggregator, request_data, mode)
        if len(menus) == 1:
            menu = menus[0]
            try:
                ProductMenu.objects.filter(menu=menu).delete()
            except Exception as e:
                exception = "Menu with id {} not created or updated in Aggregator {} because error deleting product asociated, details: {}".format(
                    menu_id, aggregator.name, e)
                insert_trace(trace_type=trace_type.value,
                             exception=exception,
                             instance_id=menu_id,
                             content=str(request_data),
                             mode=mode.value)
            menu = self.menu_ctl.update_sent_mode(menu_id, mode)
        elif len(menus) > 1:
            exception = "Exist more than a Menu with id {}, Aggregator {} and Store {}".format(menu_id,
                                                                                               aggregator.name,
                                                                                               store.integrationId)
            insert_trace(trace_type=trace_type.value,
                         exception=exception,
                         instance_id=menu_id,
                         content=str(request_data),
                         mode=mode.value)
            return Response(data={'message': MANY_EXIST_MENU_BY_PARAMS, 'details': ''},
                            status=status.HTTP_400_BAD_REQUEST)

        self.product_menu_ctl.insert_product_rappi(request_data.get('items', []), menu, mode)

        url = base_url + '/menu'

        try:
            response = requests.post(url=url, headers=headers, data=json.dumps(request_data))
        except Exception as e:
            exception = "Menu with id {} not created or updated in Aggregator {}, details: {}".format(menu_id,
                                                                                                      aggregator.name,
                                                                                                      e)
            insert_trace(trace_type=trace_type.value,
                         exception=exception,
                         instance_id=menu_id,
                         content=str(request_data),
                         mode=mode.value)
            return Response(data={'message': NOT_UPDATE_MENU, 'details': str(e)},
                            status=status.HTTP_400_BAD_REQUEST)

        if response.status_code == status.HTTP_200_OK:
            exception = "Menu with id {} created or updated success in Aggregator {}".format(aggregator.name, menu_id)
            insert_trace(trace_type=trace_type.value,
                         exception=exception,
                         instance_id=menu_id,
                         content=str(request_data),
                         mode=mode.value)

            body = {'message': UPDATE_MENU_SUCCESS, 'details': ''}
            return Response(data=body, status=response.status_code)
        else:
            exception = "Menu with id {} not created or updated in Aggregator {} because bad status response received, details: {}".format(
                menu_id, aggregator.name, str(response.content))
            insert_trace(trace_type=trace_type.value,
                         instance_id=menu_id,
                         content=str(request_data),
                         exception=exception,
                         mode=mode.value)
            body = {'message': NOT_UPDATE_MENU, 'details': str(response.content)}
            return Response(data=body, status=response.status_code)

    # def menu_update_urls(self, store_id, menu_id, headers):
    #     url = settings.API_GLOVO_PRODUCTION_URL + '/webhook/stores/{}/menu'.format(store_id)
    #     response = requests.post(url, headers=headers, data={'menuUrl': self.generate_menu_url(menu_id)})
    #     return response

    # def generate_menu_url(self, menu_id):
    #     return settings.API_HOST + '/api/menu_app/menu/all/data/{}/'.format(menu_id)
