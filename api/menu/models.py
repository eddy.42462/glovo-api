from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import JSONField
from store.models import Store, Aggregator


class Menu(models.Model):
    menu_id = models.CharField(primary_key=True, max_length=64)
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    transaction_id = models.CharField(max_length=64, null=True)  # este atributos es de glovo
    status_in_aggregator = models.IntegerField(null=True)
    updated_at = models.DateTimeField(auto_now=True)  # para saber la fecha de la ultima actualización
    created_at = models.DateTimeField(auto_now_add=True)  # para saber la fecha que se ingreso por promera.
    menu_json = JSONField()
    sent_production = models.BooleanField(default=False)
    sent_develop = models.BooleanField(default=False)
    aggregator = models.ForeignKey(Aggregator, on_delete=models.CASCADE)

    def __str__(self):
        return self.menu_id


class ProductMenu(models.Model):
    id_pk = models.AutoField(primary_key=True)
    id = models.CharField(max_length=64, null=True)
    name = models.CharField(max_length=64)
    price = models.CharField(max_length=32)
    image_url = models.CharField(max_length=255, null=True, blank=True)
    description = models.CharField(max_length=1024, null=True, blank=True)
    available = models.BooleanField(null=True)
    rappi_ids = ArrayField(models.CharField(max_length=64), null=True)
    sku = models.CharField(max_length=64, null=True, blank=True)
    stock = models.CharField(max_length=64, null=True, blank=True)
    sorting_position = models.IntegerField(null=True)
    type = models.CharField(max_length=64, null=True, blank=True)
    max_limit = models.PositiveIntegerField(null=True)
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Image(models.Model):
    id_pk = models.AutoField(primary_key=True)
    format = models.CharField(max_length=4)
    product_id = models.CharField(max_length=64, null=False)
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    image_byte = models.BinaryField(null=False)
    created_at = models.DateTimeField(auto_now_add=True)
