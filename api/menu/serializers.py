from django.db import IntegrityError
from jsonschema import ValidationError
from rest_framework import serializers
from store.serializers import StoreSerializer, AggregatorSerializer

from .models import *


class BulkCreateUpdateListSerializer(serializers.ListSerializer):

    def create(self, validated_data):
        result = [self.child.create(attrs) for attrs in validated_data]
        try:
            self.child.Meta.model.objects.bulk_create(result)
        except IntegrityError as e:
            raise ValidationError(e)
        return result

    def update(self, instances, validated_data):
        instance_hash = {index: instance for index, instance in enumerate(instances)}
        result = [
            self.child.update(instance_hash[index], attrs)
            for index, attrs in enumerate(validated_data)
        ]
        writable_fields = [
            x
            for x in self.child.Meta.fields
            if x not in self.child.Meta.read_only_fields
        ]
        try:
            self.child.Meta.model.objects.bulk_update(result, writable_fields)
        except IntegrityError as e:
            raise ValidationError(e)
        return result


class BulkImageSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        instance = Image(**validated_data)
        if isinstance(self._kwargs["data"], dict):
            instance.save()
        return instance

    def update(self, instance, validated_data):
        instance.product_id = validated_data["product_id"]
        instance.image_data = validated_data["image_data"]
        instance.image_byte = validated_data["image_byte"]
        instance.menu_id = validated_data["menu_id"]
        if isinstance(self._kwargs["data"], dict):
            instance.save()
        return instance

    class Meta:
        model = Image
        fields = ('product_id', 'image_byte', 'menu_id')
        list_serializer_class = BulkCreateUpdateListSerializer


class ProductMenuSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model ProductMenu
    """
    rappi_ids = serializers.ListField(child=serializers.CharField())

    class Meta:
        model = ProductMenu
        fields = ('id', 'name', 'price', 'image_url', 'description', 'available',
                  'rappi_ids', 'sku', 'stock', 'sorting_position', 'type')


class MenuSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer of the model Menu
    """
    store = StoreSerializer(read_only=True)
    store_id = serializers.PrimaryKeyRelatedField(write_only=True,
                                                  queryset=Store.objects.all(),
                                                  source='store')
    aggregator = AggregatorSerializer(read_only=True)
    aggregator_id = serializers.PrimaryKeyRelatedField(write_only=True,
                                                       queryset=Aggregator.objects.all(),
                                                       source='aggregator')
    products = ProductMenuSerializer(many=True, read_only=True)

    class Meta:
        model = Menu
        fields = ('menu_id', 'store', 'store_id', 'status_in_aggregator', 'transaction_id', 'updated_at', 'created_at',
                  'menu_json', 'aggregator', 'aggregator_id', 'sent_production', 'sent_develop', 'products')
